<?php

if(!function_exists("encrypt")){
    function encrypt/*_php56*/($string){
        $CI =& get_instance();
        $CI->load->library("encrypt");
        return $CI->encrypt->encode($string);
    }
}

if(!function_exists("decrypt")){
    function decrypt/*_php56*/($string){
        $CI =& get_instance();
        $CI->load->library("encrypt");
        return $CI->encrypt->decode($string);
    }
}

/*if(!function_exists("encrypt")){
    function encrypt($string){
        $CI =& get_instance();
        $CI->load->library("opensslencrypt");
        return $CI->opensslencrypt->text_encrypt($string);
    }
}

if(!function_exists("decrypt")){
    function decrypt($string){
        $CI =& get_instance();
        $CI->load->library("opensslencrypt");
        return $CI->opensslencrypt->text_decrypt($string);
    }
}*/

if(!function_exists('is_openssl_encrypt')){
    function is_openssl_encrypt()
    {
        /* digunakan jika versi php 7 */
        $ci =& get_instance();
        $ci->load->library('opensslencrypt');
        $sample_user = get_detail_data("super_users", "username = 'admin'", 'password');
        decrypt($sample_user['password']);
        if(!$ci->opensslencrypt->is_openssl_encrypt()){
            $list_users = get_all_data("users", "username, password");
            $ci->db->trans_begin();
            foreach ($list_users as $key => $value) {
                $encrypt_pass = encrypt($value['username']);
                $ci->db->set("password", $encrypt_pass)->where("username", $value['username'])->update("users");
            }

            if ($ci->db->trans_status() === FALSE) $ci->db->trans_rollback();
            else $ci->db->trans_commit();

            $list_users = get_all_data("super_users", "username, password");
            $ci->db->trans_begin();
            foreach ($list_users as $key => $value) {
                $encrypt_pass = encrypt($value['username']);
                $ci->db->set("password", $encrypt_pass)->where("username", $value['username'])->update("super_users");
            }

            if ($ci->db->trans_status() === FALSE) $ci->db->trans_rollback();
            else $ci->db->trans_commit();
        }
        // else echo "belum di enkrip";
        // echo " -> ".((int) $ci->opensslencrypt->is_openssl_encrypt());
    }
}


if (!function_exists('real_escape_string')) {
    function real_escape_string($string)
    {
        return preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
        
    }
}

if(!function_exists("indonesian_date"))
{
    function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
        if (trim ($timestamp) == '')
        {
                $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
            $timestamp = strtotime ($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
            '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
            '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
            '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
            '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
            '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
            '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
            '/April/','/June/','/July/','/August/','/September/','/October/',
            '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
            'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
            'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
            'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
            'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return $date;
    } 
}

if(!function_exists("romanic_number")){
    function romanic_number($integer, $upcase = true) 
    { 
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
        $return = ''; 
        while($integer > 0) 
        { 
            foreach($table as $rom=>$arb) 
            { 
                if($integer >= $arb) 
                { 
                    $integer -= $arb; 
                    $return .= $rom; 
                    break; 
                } 
            } 
        } 

        return $return; 
    } 
}

if(!function_exists("print_pdf")){
    function print_pdf($content, $file_name="SuratKeluar")
    {
        $CI =& get_instance();
        $CI->load->library("pdf");
        $CI->pdf->generate_pdf($content, "$file_name.pdf");
    }
}

/*'<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 100%;\">
    <tbody>
        <tr>
            <td style=\"width: 30px; text-align: left; vertical-align: top;\">6.1</td>
            <td colspan=\"2\" rowspan=\"1\" style=\"width: 1076px;\">Test alkshdashdkjahskjdh;lqwjeklhkjasgdhjkagsdkjashdlkjashdkjashgdkjagsdkjasgdkjagd<br />
            &nbsp;</td>
        </tr>
        <tr>
            <td style=\"width: 30px;\">&nbsp;</td>
            <td style=\"width: 52px; text-align: left; vertical-align: top;\">6.1.1</td>
            <td style=\"width: 1018px;\">hasgdjhgaskjdgajkhsgdjgj</td>
        </tr>
        <tr>
            <td style=\"width: 30px;\">&nbsp;</td>
            <td style=\"width: 52px; text-align: left; vertical-align: top;\">6.1.2</td>
            <td style=\"width: 1018px;\">asjgdkjagsdkajsgdjakgdkajsgdk</td>
        </tr>
        <tr>
            <td style=\"width: 30px;\">&nbsp;</td>
            <td style=\"width: 52px; text-align: left; vertical-align: top;\">6.1.3</td>
            <td style=\"width: 1018px;\">asjdgkagoiqwyeo</td>
        </tr>
        <tr>
            <td style=\"width: 30px;\">&nbsp;</td>
            <td style=\"width: 52px; text-align: left; vertical-align: top;\">&nbsp;</td>
            <td style=\"width: 1018px;\">&nbsp;</td>
        </tr>
        <tr>
            <td style=\"width: 30px; text-align: left; vertical-align: top;\">6.2</td>
            <td colspan=\"2\" rowspan=\"1\" style=\"width: 1076px; text-align: left; vertical-align: top;\">akgsdasdkjgasjkdgkjgqkdjgakjsdg<br />
            <strong>Tahapan:</strong></td>
        </tr>
    </tbody>
</table>'*/

if(!function_exists("get_detail_list")){
    function get_detail_list($table, $condition="1", $col="*", $start=0, $limit=-1, $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null")
    {
        $CI =& get_instance();
        
        return $CI->gm->direct_detail_list($table, $condition, $col, $start, $limit, $to_object, $order, $join, $pkjoin, $group);
        // return json_detail_list($table, $condition, $col, $start, $limit, $order, $group);
    }
}


if(!function_exists("get_detail_data")){
    function get_detail_data($table, $condition, $col="*", $start=0, $limit=-1)
    {
        $CI =& get_instance();
        
        return $CI->gm->direct_detail_data($table, $condition, $col, $start, $limit);
        // return search_in_db($table, $condition, $col, $start, $limit)[0];

    }
}

if (!function_exists("get_count_data")) {
    function get_count_data($table, $condition)
    {
        $CI =& get_instance();
        $attributes = $CI->gm->direct_detail_data($table, $condition, "count(*) 'total'");
        $data = (string)$attributes['total'];
        return $data;
        // return search_in_db($table, $condition, $col, $start, $limit)[0];
    }
}


if(!function_exists("get_all_data")){
    function get_all_data($table, $col="*", $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null", $start=0, $limit=-1)
    {
        $CI =& get_instance();
        
        return $CI->gm->direct_all_data($table, $col, $to_object, $order, $join, $pkjoin, $group, $start, $limit);
    }
}

if(!function_exists('generateRandomString')){
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
}

if(!function_exists('generate_secret_key')){
    function generate_secret_key($text)
    {
        return generateRandomString(20)."X$text"."X".generateRandomString(20);
        // return base64_encode(generateRandomString(15)."X$text"."X".generateRandomString(15));
    }
}

if(!function_exists('decode_str')){
    function decode_str($str)
    {
        $str = base64_decode($str);
        $f1 = strpos($str, "$");

        $afterf1 = substr($str, $f1+1);
        $f2 = strpos($afterf1, "$");
        $real_pass = substr($afterf1, 0, strpos($afterf1, "$"));
        
        return base64_decode($real_pass);
    }
}

if (!function_exists('get_message')) {
    function get_message($type, $lang="ENGLISH")
    {
        $attributes = get_detail_data('MA0016', "MSG_TYPE = '$type'", "MSG_$lang");
        $data = (string)$attributes["MSG_$lang"];
        return $data;
    }
}


if(!function_exists("showit")){
    function showit($view, $data=array())
    {
        $CI =& get_instance();
        $data['content'] = $view;
        $CI->load->view("index", $data);
    }
}

if (! function_exists('insert_data')) {

    /**
     * For Select General.
     *
     * @param $action
     *
     */
    function insert_data($table, $data)
        {
            
            $CI =& get_instance();
            $table_engine = check_table_engine($table);
            if(strtolower($table_engine) == "innodb") $CI->db->trans_begin();
            $exec = $CI->db->insert($table, $data);
            $new_id = $CI->db->insert_id();
            $uri1 = get_segment_uri(1);
            $uri1 = ucwords(str_replace('_', ' ', $uri1));
            logActivity("melakukan penambahan data $uri1 dengan id : $new_id");
            if(strtolower($table_engine) == "innodb"){
                if ($CI->db->trans_status() === false) {
                    $CI->db->trans_rollback();
                    return -1;
                }
                else {
                    $CI->db->trans_commit();
                    return $new_id;
                }
            }
            else {
                if($exec) return $new_id;
                else return -1;
            }
            // if($exec) return $new_id;
            // else 
        }
}

if (! function_exists('insert_multiple_data')) {

    /**
     * For Select General.
     *
     * @param $action
     *
     */
    function insert_multiple_data($table, $data)
        {
            $CI =& get_instance();

            $CI->db->insert_batch($table, $data);
            // return $CI->db->insert_id();
        }
}

if (!function_exists('check_table_engine')) {

    /**
     * For Select General.
     *
     * @param $action
     *
     */
    function check_table_engine($table)
    {
        $CI =& get_instance();
        $query = "SHOW TABLE STATUS WHERE NAME = '$table'";
        $res = $CI->db->query($query)->row_array();
        return $res['Engine'];
    }
}

if (! function_exists('update_data')) {

    /**
     * For Select General.
     *
     * @param $action
     *
     */
    function update_data($table, $data, $data_id)
        {
            $CI =& get_instance();
            $col_list = "";
            foreach ($data as $key => $value) {
                $col_list .= "$key, ";
            }
            $col_list = substr($col_list, 0, -2);
            $data_awal = get_detail_data($table, $data_id, $col_list);
            $diff_col = checking_column($data_awal, $data, $col_list);
            $table_engine = check_table_engine($table);
            if(strtolower($table_engine) == "innodb") $CI->db->trans_begin();
            $exec = $CI->db->update($table, $data, $data_id);
            logActivity("melakukan perubahan data $uri1 dengan id : $new_id pada $diff_col");
            if(strtolower($table_engine) == "innodb"){
                if ($CI->db->trans_status() === false) {
                    $CI->db->trans_rollback();
                    return -1;
                }
                else {
                    $CI->db->trans_commit();
                    return $CI->db->affected_rows();
                }
            }
            else{
                if($exec) return $CI->db->affected_rows();
                else return -1;
            }
        }
}

if (!function_exists('generate_data_for_datatable')) {
    function generate_data_for_datatable($table, $kondisi='', $order='', $group='')
    {
        $ci =& get_instance();
        $get_array_tab = apiarraytab();

        /* Untuk mendapatkan 10 data saja */
        // $dt = array('table' => $table, 'limit' => $get_array_tab['limit'], 'start' => $get_array_tab['start'], 'condition' => $kondisi, 'datatable' => true);
        // $result = get_data($dt, true);

        $ci->rpdb->select('count(*) total');
        if($kondisi!= '') $ci->rpdb->where($kondisi);
        if($group!= '') $ci->rpdb->select("count(distinct($group)) 'total'");
         $r2 = $ci->rpdb->datatable_result($table);


        $ci->rpdb->select('*')->where($kondisi)->limit($get_array_tab['limit'], $get_array_tab['start']);
        if($kondisi!= '') $ci->rpdb->where($kondisi);
        if($group!= '') $ci->rpdb->group_by($group);
        if($order!= '') $ci->rpdb->order_by($order);
        $result = $ci->rpdb->datatable_result($table, true);
        
        if($result->data != null && $r2['data'] != null){
            $result->recordsTotal = $r2['data'][0]['total'];
            $result->recordsFiltered = $r2['data'][0]['total'];
            $result->query2 = $r2['query'];
        }
        
        return $result;
    }
}

if(!function_exists("getallsession")){
    function getallsession(){
        $CI =& get_instance();
        return $CI->session->all_userdata();
    }
}

if (!function_exists("call_modal")) {
    function call_modal($modal_title, $form, $process_link, $data=array(), $js_file="")
    {
        $CI = &get_instance();
        $data['modal_title'] = $modal_title;
        $data['form'] = $form;
        $data['link'] = $process_link;
        if($js_file !== "") $data['js_modal'] = $js_file;
        $CI->load->view('modal/main', $data);
        
    }
}

if(!function_exists("show_data")){
    function show_data($arr_data, $exit=false){
        echo "<pre>";
        print_r($arr_data);
        echo "</pre>";
        if($exit) exit;
    }
}

if(!function_exists("ajax_validate")){
    function ajax_validate(){
        $CI = &get_instance();
        return $CI->input->is_ajax_request() ? true:false;
    }
}

if (!function_exists("checking_column")) {
    function checking_column($old, $new, $col)
    {
        $CI = &get_instance();
        $list_kolom = explode(",", $col);
        $yang_berubah = array();
        foreach ($list_kolom as $key => $value) {
            $nama_kolom = trim($value);
            $nama_kolom1 = ucwords(str_replace("_", " ", $nama_kolom));
            if ($old[$nama_kolom] != $new[$nama_kolom]) $yang_berubah[] = "kolom $nama_kolom1 dari $old[$nama_kolom] menjadi $new[$nama_kolom]";
        }

        $list_yang_berubah = "";
        if ($yang_berubah != null) {
            $index = 0;
            $total_data = count($yang_berubah);
            foreach ($yang_berubah as $key => $value) {
                $list_yang_berubah .= $value;
                if ($index < ($total_data - 1)) $list_yang_berubah .= ", ";
                $index++;
            }
        }
        return $list_yang_berubah;
    }
}

if(!function_exists("check_ajax_process")){
    function check_ajax_process(){
        $CI = &get_instance();
        if(!$CI->input->is_ajax_request()) exit("Sorry you're not authorized to Access this Page");
    }
}

if(!function_exists("logActivity")){
    function logActivity($keterangan, $status = true, $uname=""){
        date_default_timezone_set("Asia/Jakarta");
        $CI =& get_instance();
        $CI->load->library('user_agent');
            $agent = $CI->agent;
            $agent_string = $agent->agent_string();
            $platform = $agent->platform();
            $browser = $agent->browser();
            $version = $agent->version();
            $robot = $agent->robot();
            $mobile = $agent->mobile();
            $referrer = $agent->referrer();
            $is_referral = $agent->is_referral();
            $agent_desc = "$platform; $browser $version";
            if($mobile != "") $agent_desc .= "; $mobile";
            if($robot != "") $agent_desc .= "; $robot";
            // $languages = $agent->languages();
            // $result['charsets'] = $agent->charsets();
        $username = $uname != "" ? $uname : $CI->session->userdata('username');
        $tgl = date('Y-m-d');
        $waktu = date('H:i:s');
        $dt = array(
            "username"      => $username,
            "aktivitas"     => $keterangan,
            "tanggal"       => $tgl,
            "waktu"         => $waktu,
            "ip_address"    => $_SERVER['REMOTE_ADDR'],
            "user_agent"    => $agent_desc,
            "url"           => $referrer,
            "status"        => $status
        );

        $CI->db->insert('log_users', $dt);
    }
}

if(!function_exists("get_segment_uri")){
    function get_segment_uri($index){
        $CI =& get_instance();
        
        return $CI->uri->segment($index);
    }
}

if(!function_exists("waktu_proses")){
    function waktu_proses($awal){
        $akhir = microtime(true);
        $proses = $akhir - $awal;
        return $proses;
    }
}

if(!function_exists('generate_new_key')){
    function generate_new_key($old_key, $unset=false)
    {
        $new_key = substr($old_key, 0, strpos($old_key, "-"));
        $_POST[$new_key] = $_POST[$old_key];
        if($unset) unset($_POST[$old_key]);
    }
}

if(!function_exists('strposa')){   
    function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            // stop on first true result
            if(strpos($haystack, $query, $offset) !== false) return true; 
        }
        return false;
    }
}

if(!function_exists("set_validation")){
    function set_validation($data){
        $CI =& get_instance();
        $CI->load->library('form_validation');
        $CI->load->helper('security');
        $data = xss_clean($data);
        
        $CI->form_validation->set_message('required', get_message("required_validation"));
        $CI->form_validation->set_message('min_length', get_message("min_length_validation"));
        $CI->form_validation->set_message('valid_email', get_message("valid_email_validation"));
        $CI->form_validation->set_message('numeric', get_message("numeric_validation"));
        $CI->form_validation->set_message('alpha', get_message("alpha_validation"));
        $CI->form_validation->set_message('matches', get_message("matches_validation"));
        // show_data($data);
        // show_data($_POST);
        foreach ($data as $key => $value) {
            $alt = $key;
            $altChange = false;
            $validation = "";
            $alreadyGenerateNewKey = false;

            if(strpos($key, "-nr") > 0) {
                if (!$alreadyGenerateNewKey) {
                    generate_new_key($key, true);
                    $alreadyGenerateNewKey = true;
                }
                continue;
            }
            if(is_array($value)){
                // $pecah = explode(":", explode('-', $key)[1]);
                // $alt = $pecah[1];
                // generate_new_key($key, "-", true);
                continue;
            }
            if(strpos($key, "-alt:") > 0){
                /* echo  */$startAlt = substr($key, strpos($key, "-alt:")+strlen("-alt:")); //echo " -> ";
                $alt = substr($startAlt, 0, ((strpos($startAlt, "-") !== false) ? strpos($startAlt, "-") : strlen($startAlt)));
                $altChange = true;
                // $alt = $pecah[1];
                if(!$alreadyGenerateNewKey){
                    generate_new_key($key, true);
                    $alreadyGenerateNewKey = true;
                }

                $key = substr($key, 0, strpos($key, '-'));
            } 
            // echo "$key, ";
            if (strpos($key, "-valid:") > 0) {
                $tmp = explode("-", $key);
                $listValidation = array();
                foreach ($tmp as $kt => $vt) {
                    if($kt == 0 || strpos($vt, "valid") === false) continue;
                    $listnya = substr($vt, strpos($vt, ':')+1);
                    $listValidation = explode(";", $listnya);
                }
                foreach ($listValidation as $kv => $vv) {
                    if($vv == "") continue;
                    if(strpos($vv, '=') !== false){
                        $pecahVV = explode("=", $vv);
                        $validation .= $pecahVV[0]."[$pecahVV[1]]|";
                    }
                    else $validation .= "$vv|";
                }
                $alt = (!$altChange) ? $tmp[0] : $alt;
                if (!$alreadyGenerateNewKey) {
                    generate_new_key($key, true);
                    $alreadyGenerateNewKey = true;
                }
                $secondElement2 = $tmp[0];
                $key = (!$altChange) ? $secondElement2: $key;
            }
            $validation .= 'required|trim|xss_clean';
            // echo "$alt -> $validation<br>";
            // if($key == "isi_surat") {
            //     // echo "$key --> just required";
            //     $validation = "required";
            // }
            $CI->form_validation->set_rules($key,"<b style='color: #333'>".ucwords(str_replace("_", " ", $alt))."</b>", $validation);
            // $CI->form_validation->set_message("required", "Field ".str_replace("_", "", $key)." tidak boleh kosong !");
        }
        unset($_POST[0]);
        // show_data($_POST);
        if($CI->form_validation->run()) return true;
        else return $CI->form_validation->error_array();
        // else return validation_errors();
    }
}

if (!function_exists('check_array_input')) {
    function check_array_input()
    {
        foreach ($_POST as $key => $value) {
            if(strpos($key, "[]") > 0){
                generate_new_key($key, "-", true);
            }
        }
        foreach ($_POST as $key => $value) {
            if(!is_array($value)) continue;
            else{
                foreach ($value as $key_val => $subval) {
                    $isi_array = trim($subval);
                    if ($isi_array == "" || $isi_array == null) {
                        return $key;
                    }
                }
            }
        }
        return true;
    }
}

if (!function_exists('check_validation')) {
    function check_validation()
    {
        $check_array = check_array_input();
        $output = array();
        if(!is_bool($check_array)){
            $output[$check_array] = "<p>Pastikan semua <i>Field</i> <b style='color: #333'>$check_array</b> tidak <b style=\'color: #333\'>kosong</b> !</p>";
        }
        $check_non_array = set_validation($_POST);
        // show_data($check_array);
        // show_data($check_non_array);
        if(!is_bool($check_non_array)) {

            $response['status'] = -1;
            $output = array_merge($check_non_array, $output);
            // show_data($output);
            // $response['message'] = $input;
            // echo validation_errors();
            $response['message'] = "";
            $response['error_fields'] = array();
            $index=0;
            foreach ($output as $key => $value) {
                $response['message'] .= "<p>$value</p>";
                $response['error_fields'][$index] = $key;
                if(strpos($key, "-alt:") > 0){
                    $pecah = explode("-", $key);
                    $response['error_fields'][$index] = $pecah[0];
                }
                $index++;
            }
            echo json_encode($response);
            exit;
        }
        // else{
        //     $check_array = check_array_input();
        //     if(!is_bool($check_array)){
        //         $response['status'] = -1;
        //         $response['message'] = "<p>Pastikan semua <i>Field</i> <b style='color: #333'>$check_array</b> tidak <b style=\'color: #333\'>kosong</b> !</p>";
        //         echo json_encode($response);
        //         exit;
        //     }
        // }
    }
}

if(!function_exists("validate_field")){
    function validate_field($table, $data)
    {
        $CI =& get_instance();
        $q = "SHOW COLUMNs from $table";
        $exec = $CI->db->query($q);
        $hasil = $exec->result_array();
        $exec->free_result();
        $list_field = array();
        foreach ($hasil as $col) {
            $list_field[] = $col['Field'];
        }

        foreach ($data as $key => $value) {
            if(!in_array($key, $list_field)) unset($data[$key]);
        }
        return $data;
    }
}

if(!function_exists("set_data")){
    function set_data($table, $type="insert"){
        $CI =& get_instance();
        $q = "SHOW COLUMNs from $table";
        $exec = $CI->db->query($q);
        $hasil = $exec->result_array();
        $exec->free_result();
        $field_tgl_now = array('create_date', 'created_date');
        $field_created = array('create_date', 'created_date', 'create_by', 'created_date');
        $field_updated = array('update_date', 'updated_date', 'update_by', 'updated_date');
        $field_create_update = array_merge($field_created, $field_updated);
        // print_r($hasil);
        $data = array();
        foreach ($hasil as $key => $value) {
            
            if($value['Field'] == "isi_surat") $val_data = $CI->input->post($value['Field']);
            else $val_data = $CI->input->post($value['Field'], true);
            
            if($val_data == "" || !isset($val_data)) continue;
            if($type == 'update' && in_array($value['Field'], $field_created)) continue;
            else if($type == 'insert' && in_array($value['Field'], $field_updated)) continue;

            if(in_array($value['Field'], $field_tgl_now)) $data[$value['Field']] = date("Y-m-d H:i:s");
            elseif ($value['Field'] == 'tgl_surat') $data[$value['Field']] = date("Y-m-d", strtotime($val_data));
            else {
                if(is_array($val_data)) {
                    $arr_data = "";
                    $x=0;
                    foreach ($val_data as $row) {
                        if($row == "") continue;
                        $arr_data .= $row;
                        if($x != (count($val_data)-1)) $arr_data .= "; ";
                        $x++;
                    }
                    $val_data = $arr_data;
                }
                elseif (in_array($value['Field'], $field_create_update)) $val_data = get_session("username");
                $data[$value['Field']] = hilangkan_petik($val_data);
            }
            
        }
        // print_r($data);
        return $data;
        // return $CI->form_validation->run();
    }
}

if(!function_exists("generate_id")){
    function generate_id($table){
        $id = date('dmyHis');
        $dt = get_data($table, "all", array('no_urut'), '', false);

        return $id;
        
    }
}


if (!function_exists('convert_to_html')) {
    /* Especially for Reference (Surat, Dispo, Nota Dinas) */
    function convert_to_html($data, $delim="; ")
    {
        if(strpos($data, '; ') > 0){
            $dt = str_replace($delim, "</li><li>", trim($data));
            $result = "<ol><li>$dt</li></ol>";    
        }
        else $result = $data;
        return $result;
    }
}

if(!function_exists("array_to_string")){
    function array_to_string($arr_value, $using = ",", $to_db = false){
        if(!isAssoc($arr_value)) $result = implode($using, $arr_value);
        else {
            $x=0;
            $result = "";
            foreach ($arr_value as $key => $val) {
                if(($x++) != 0) $result .= $using;
                $result .= "$key = ";
                if(is_int($val)) $result .= $val;
                else if ($val == null || $val == 'null') $result .= "null";
                else $result .= "'$val'";
            }
        }
        if($to_db) {
            $result = "";
            $x=0;
                // echo " <br>";

            foreach ($arr_value as $val) {
                // echo "$val <br>";
                if(($x++) != 0) $result .= $using;
                if(is_int($val)) $result .= $val;
                else if ($val == null || $val == 'null') $result .= "null";
                else $result .= "'$val'";
            }
        }
        return $result;
    }
}

if(!function_exists('isAssoc')){
    function isAssoc($arr)
    {
        // if (array() === $arr || !isAssoc($arr)) return false;
        // return array_keys($arr) !== range(0, count($arr) - 1);
        return ($arr !== array_values($arr));
    }
}

if(!function_exists("show_content")){
    function show_content($view_name, $data=array()){
        $CI =& get_instance();
        $data['main_content'] = isset($data['main_content']) ? $data['main_content'] : "content";
        $data['js_under_tambahan'] = isset($data['js_under_tambahan']) ? $data['js_under_tambahan'] : "js_under";
        $data['css_head_tambahan'] = isset($data['css_head_tambahan']) ? $data['css_head_tambahan'] : "css_head";
        $CI->load->view($view_name, $data);
    }
}

if(!function_exists("hilangkan_petik")){
    function hilangkan_petik($value){
        return str_replace("'", "\'", str_replace("\\", "", $value));
    }
}

if(!function_exists("set_session")){
    function set_session($param1, $param2=""){
        $CI =& get_instance();
        if(is_array($param1)) $CI->session->set_userdata($param1);
        else $CI->session->set_userdata($param1, $param2);
    }
}

if(!function_exists("destroy_session")){
    function destroy_session(){
        $CI =& get_instance();
        session_unset();
        session_destroy();
        // $CI->session->sess_destroy();
        // unset($_SESSION);
        // session_destroy();
    }
}

if(!function_exists("unset_session")){
    function unset_session($sess_name){
        $CI =& get_instance();
        /*if(is_array($param1)) */$CI->session->unset_userdata($sess_name);
        // else $CI->session->set_userdata($sess_name);
    }
}

if(!function_exists("get_session")){
    function get_session($sess_name){
        $CI =& get_instance();
        return $CI->session->userdata($sess_name);
    }
}
?>