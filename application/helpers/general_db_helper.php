<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Helper to track several things.
 * 
 * @copyright 	Prime Pratama
 * @link		https://admin.demafstuinjkt.com
 */

if (! function_exists('get_data')) {

    /**
     * For Select General.
     *
     * @param $action
     *
     */
	// function get_data(/*$table, */$data_table = array(), /*, $condition = "all", $column = "*",*/ $get_total_rows=false/*, $join="null", $pk_join="null", $order="null", $group="null"*/)
 //        {
    function get_data($data_table = array(), $to_object = false, $sp_name='', $get_total_rows=false)
        {
            // $CI =& get_instance();
            $sp_name = ($sp_name == '') ? SP_UNIVERSAL : $sp_name;
            $calling_su = ($sp_name == SP_UNIVERSAL) ? true : false;
            $is_datatable = isset($data_table['datatable']) ? true: false;
            if(!$calling_su) unset($data_table['datatable']);
            $proc_param = validate_param($data_table, $calling_su);
            if($is_datatable)
            {
                $list_data = exec_sp($sp_name, $proc_param, $to_object, false, true);
                $total_row = exec_sp($sp_name, $proc_param, $to_object, true);
                if ($to_object) {
                    $result = new stdClass();
                    
                    $result->data = $list_data->data;
                    $result->recordsFiltered = $total_row->data;
                    $result->recordsTotal = $total_row->data;
                    $result->query = $list_data->query;
                    $result->status = true;
                }
                else $result = array('data' => $list_data['data'], 'recordsTotal' => $total_row['data'], 'recordsFiltered' => $total_row['data'], 'query' => $list_data['query'], 'status' => true);
                return $result;
            }
            else return exec_sp($sp_name, $proc_param, $to_object, $get_total_rows);
        }
}

function getlimit(){
    $ci = & get_instance();
    $mth= $_SERVER['REQUEST_METHOD'] ;
    $x=$ci->input->get('limit');
    if(strtolower($mth)=="post"){
        $x=$ci->input->post('limit');
    }

 
     if($x){

    if ($x>5000){
        return 5000;
    }else{
        return $x;
    }

    }else{
        return 10;
        }
}
function getstart(){
    $ci = & get_instance();
    $mth= $_SERVER['REQUEST_METHOD'] ;

    $x=$ci->input->get('start');
    if(strtolower($mth)=="post"){
        $x=$ci->input->post('start');
    }
/*    $y=$x -1;
    $x=($y <= 0 ? 0 : $y * getlimit() );*/

    if($x){
    return $x;
    }else{
        return 0;
        }

}

function authUser(){
    $is_login = (int) get_session("logged_in");
    // $x = getallsession();
    // show_data($x);
    // echo "hasil : ".(isset($is_login) ? "kosong $is_login" : "isi");
    if (!$is_login){
        $param_url = get_segment_uri(1);
        if($param_url != "auth") redirect(base_url().'auth/guest');
    }

}

function generateNewId()
{
    $Date1              = date("Y-m-d H:i:s");
        $ran                = rand(10000,99999);
        return strtotime($Date1)+$ran;
}

if(!function_exists('exec_cud')){
    function exec_cud($dt=array())
    {
        $data_update = str_replace("'", "\'", array_to_string($dt['data'], ","));
        $command = (isset($dt['command'])) ? str_replace(",", ";", $dt['command']) : "null";
        $table = (isset($dt['table'])) ? str_replace(",", ";", $dt['table']) : "null";
        // $data = (isset($dt['data'])) ? $dt['data'] : "null";
        if(isset($dt['data'][0])){
            foreach ($variable as $key => $value) {
                # code...
            }
        }
        $id_column = (isset($dt['pk'])) ? $dt['pk'] : "null";
        $param = array($dt['command'], $dt['table'], $data_update);
        $proc_param = array_to_string(array());
        // return exec_sp("cud_universal", $)
    }
}

// if (!function_exists('convert_to_string')) {
//     # code...
// }


if(!function_exists('validate_param')){
    function validate_param($param=array(), $for_su=true)
    {
        if($for_su){
            $new_column = "*";
            if(isset($param['column'])){
                if($param['column'] != "*"){
                    $new_column = "";
                    if(is_array($param['column'])){
                        for ($i=0; $i < count($param['column']); $i++) { 
                            if($i == count($param['column'])-1) $new_column .= $param['column'][$i]." ";
                            else $new_column .= $param['column'][$i].", ";
                        }
                    }
                    else $new_column = $param['column'];
                }
            }
            
            // else $q .= "* ";

            // $q .= " from ";
            // $q .= $condition == "all" ? "$table" : "$table where $condition";
            $table = str_replace("'", "\'", $param['table']);
            $new_column = str_replace("'", "\'", $new_column);
            $condition = (isset($param['condition'])) ? str_replace("'", "\'", $param['condition']) : "null";
            $join = "";
            if(isset($param['join']) && is_array($param['join'])){
                foreach ($param['join'] as $value) {
                    if(isset($value[1])) $join .= $value[0];
                    $join .= " join $value[1], ";
                }
            }
            $join = (isset($param['join'])) ? /*str_replace(",", ";",*/ $param['join']/*)*/ : "null";
            $pk_join = (isset($param['pk_join'])) ? str_replace(",", ";", $param['pk_join']) : "null";
            $order = (isset($param['order'])) ? str_replace(",", ";", $param['order']) : "null";
            $group = (isset($param['group'])) ? str_replace("'", "\'", str_replace("|", ";", $param['group'])) : "null";
            $start_from_function = getstart();
            $limit_from_function = getlimit();
            $start = (isset($param['start'])) ? (int) $param['start'] : ((isset($start_from_function)) ? $start_from_function : 0);
            $limit = (isset($param['limit'])) ? (int) $param['limit'] : ((isset($limit_from_function)) ? $limit_from_function : 0);
            $proc_param = array($table, $condition, $new_column, $join, $pk_join, $order, $group, $start, $limit);
        }
        else $proc_param = $param;
        
            $param = array_to_string($proc_param, ", ", true);

            return $param;
    }
}

/**
 * Helper to track several things.
 * 
 * @copyright   Raditya Pratama
 * @link        https://psycode.net
 */

if (! function_exists('exec_sp')) {

    /**
     * For Executing Stored Procedure MySQL.
     *
     * @param $action
     *
     */
    function exec_sp($sp_name, $param="", $to_object = false, $only_total=false, $for_datatable=false)
        {
            // error_reporting(0);
            $CI =& get_instance();
            $q = "call $sp_name ($param)";
            $exec = $CI->db->query($q);
            $total_row = $exec->num_rows();
            if($for_datatable) {
                $result = ($to_object) ? $exec->result() : $exec->result_array();
            }
            else{
                if(!$to_object) $result = ($total_row > 1) ? $exec->result_array() : $exec->row_array();
                else $result = ($total_row > 1) ? $exec->result() : $exec->row();    
            }
            
            $lastq = $CI->db->last_query();
            $exec->free_result();
            $CI->db->freeDBResource();
            if(!$to_object) $hasil = array('data' => ($only_total) ? $total_row : $result, 'query' => $lastq);
            else {
                $hasil = new stdClass();
                $hasil->data    = ($only_total) ? $total_row : $result;
                $hasil->query   = $lastq;
            }
            // if($only_total) {
            //     if(!$to_object) $hasil['data'] = $total_row;
            //     else $hasil->data = $total_row;
            // }
            // else return $result;
            return $hasil;
        }
}





?>