<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	* 
	*/
	class Generalmodel extends CI_Model
	{
		// protected $_main_table = "surat";
		private $special_keyword = array('not in', 'in');
		function __construct()
		{
			parent::__construct();
		}

		function tambah_upload($data)
		{
			$this->db->insert("test_upload", $data);
		}

		function direct_detail_data($table, $condition, $col="*", $start=0, $limit=-1)
		{
			# code...
			$this->db->select($col)
				 ->from($table);
			if(is_array($condition)){
				foreach ($condition as $col => $val) {
					if(!is_int($col)) $this->db->where($col, $val);
					else $this->db->where($val);
				}
			}
			else $this->db->where($condition);
			if($limit >= 0) $this->db->limit($limit, $start);

			$result = $this->db->get()->row_array();
			return $result;
		}

		function get_detail_data($table, $condition, $col="*", $start=0, $limit=-1)
		{
			$result = $this->rpdb->select($col)->from($table)->where($condition)->limit($limit, $start)->get()->row_array();
			return $result;
			// $data = array(
			// 	'table' => $table,
			// 	'condition' => $condition,
			// 	'column'	=> $col,
			// 	'start'		=> $start,
			// 	'limit'		=> $limit
			// );
			// return get_data($data);
		}

		function get_last_ai($table_name) //ai = auto increment
		{
			$this->rpdb->select("`AUTO_INCREMENT` last_ai")
								->from("INFORMATION_SCHEMA.TABLES")
								->where("TABLE_SCHEMA", "adminsurat")
								->where("TABLE_NAME", $table_name);
			$tmp = $this->rpdb->get()->row_array();
			$secondElement = $tmp['last_ai'];

			$result = $secondElement;
			return $result;
		}

		function get_detail_peg($where = '', $select="") //ai = auto increment
		{
			if($where != '') $this->rpdb->where($where);
			if($select != '') $this->rpdb->select($select);
			$result = $this->rpdb->get('users_data')->row_array();
			return $result;
		}

		function get_all_peg($where = '') //ai = auto increment
		{
			if($where != '') $this->rpdb->where($where);
			$result = $this->rpdb->get('users_data')->result_array();
			return $result;
		}

		function get_all_jabatan($where = '') //ai = auto increment
		{
			if($where != '') $this->rpdb->where($where);
			$result = $this->rpdb->get('daftar_jabatan')->result_array();
			return $result;
		}

		function get_parent_jabatan($role="", $id_jabatan="")
		{
			$role_id = $role == "" ? get_session("r") : $role;
			if($role_id < 3) return null;
			$id_jabatannya = $id_jabatan == "" ? get_session("id_jabatan") : $id_jabatan;
			// return $id_jabatannya;
			$list_parentnya = array();
			$new_id_jabat = $id_jabatannya;
			while ($role_id >= 3) {
				# code...
				if($new_id_jabat == 0 || $id_jabatannya == 0) break;
				$detail_jabatan = $this->get_detail_data("daftar_jabatan", "id_jabatan = $id_jabatannya", "id_jabatan, parent, role_id");
				// show_data($detail_jabatan);
				// $list_parentnya[] = $detail_jabatan;
				$list_parentnya[] = $detail_jabatan['id_jabatan'];
				$new_id_jabat = $detail_jabatan['id_jabatan'];
				$role_id = $detail_jabatan['role_id'];
				$id_jabatannya = $detail_jabatan['parent'];
			}
			return $list_parentnya;
		}

		function get_master_data($table, $where='', $kolom="")
		{
			if ($kolom!='') $this->db->select($kolom);
			if ($where!='') $this->db->where($where);
			$result = $this->db->get($table)->result_array();
			return $result;
		}

		function get_referensi($jenis, $id)
	    {
	        $kondisi = "id_$jenis in (";
	        $x=0;
	        foreach ($id as $val) {
	            $new_val = trim($val);
	            if (($x++) > 0) {
	                $kondisi .= ", ";
	            }
	            $kondisi .= "'$new_val'";
	        }
	        $kondisi .= ")";
	        $result = $this->genmod->get_detail_list("daftar_$jenis", $kondisi);
	        $result = get_distinct_data($jenis, $result);
	        return $result;
	    }

	    function get_history_document($jenis, $id_doc, $index=0, $list_history=array()){
	    	$child_document = $this->get_child_document($jenis, $id_doc);
	    	if(!$child_document['status']) {
	    		if($index == 0) return null;
	    		else return $list_history;
	    	}
	    	else{
	    		$list_history[$index] = $child_document[0];
	    		return $this->get_history_document($jenis, $child_document[0]["id_$jenis"], $index+1, $list_history);
	    	}
	    }

	    function get_child_document($jenis, $parent_id)
	    {
	    	$result = $this->get_detail_list("daftar_$jenis", "referensi_$jenis = '$parent_id'");
	    	return get_distinct_data($jenis, $result);
	    }

	    /* Recursive Function to get document parent */
	    /* by Raditya Pratama */
	    function check_parent_document($jenis,$id_doc, $index=0, $parent_referensi=array())
	    {
	    	$detail_doc = $this->get_detail_document($jenis, $id_doc);
	    	if(!$detail_doc['status']) {
	    		if($index == 0) return null;
	    		else return $parent_referensi;
	    	}
	    	$referensinya = $detail_doc['status'] ? (isset($detail_doc[0]["referensi_$jenis"]) ? $detail_doc[0]["referensi_$jenis"] : null) : '';
	    	if($referensinya == null || $referensinya == "") {
	    		if($index == 0) if ($detail_doc != null) return $detail_doc; else return null;
	    		else {$parent_referensi[$index] = $detail_doc[0]; return $parent_referensi;}
	    	}
	    	else {
	    		$parent_referensi[$index] = $detail_doc[0];

	    		return $this->check_parent_document($jenis, $referensinya, $index+1, $parent_referensi);
	    	}
	    }


		function get_detail_document($type, $id)
		{
			$res = $this->get_detail_list("daftar_$type", "id_$type = '$id'");
			return get_distinct_data($type, $res);
		}

		function get_referensi_status($group_name, $status='', $id_klasifikasi = '')
	    {
	        $this->rpdb->where("`group`", $group_name);
	        if($status != '') $this->rpdb->where("status", $status);
	        if($id_klasifikasi != '') $this->rpdb->where("id_klasifikasi", $id_klasifikasi);
	        $result = $this->rpdb->get("master_referensi_status")->result_array();
	        
	        return $result;
	    }

	    function direct_detail_list($table, $condition="1", $col="*", $start=0, $limit=-1, $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null")
		{
			$this->db->select($col)
					 ->from($table);
			if($condition != 1)	$this->db->where($condition);
			if($limit >= 0) $this->db->limit($limit, $start);
			if($order !== "null") $this->db->order_by($order);
			if($group !== "null") $this->db->group_by($group);
			if($pkjoin !== "null" && $join !== "null") $this->db->join($join, $pkjoin);
			
			$result = $this->db->get();//->result_array();
			if($to_object) return $result->result();
			else return $result->result_array();
			// $data = array(
			// 	'table' => $table,
			// 	'condition' => $condition,
			// 	'column'	=> $col,
			// 	'order' => $order,
			// 	'join' => $join,
			// 	'pk_join' => $pkjoin,
			// 	'group' => $group,
			// 	'start'		=> $start,
			// 	'limit'		=> $limit

			// );
			// return get_data($data, $to_object);
		}

		function get_detail_list($table, $condition, $col="*", $start=0, $limit=-1, $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null")
		{
			$result = $this->rpdb->select($col)
									->from($table)
									->where($condition)
									->limit($limit, $start)
									->order_by($order)
									->group_by($group)
									->join($join, $pkjoin)
									->get();//->result_array();
			if($to_object) return $result->result();
			else return $result->result_array();
			// $data = array(
			// 	'table' => $table,
			// 	'condition' => $condition,
			// 	'column'	=> $col,
			// 	'order' => $order,
			// 	'join' => $join,
			// 	'pk_join' => $pkjoin,
			// 	'group' => $group,
			// 	'start'		=> $start,
			// 	'limit'		=> $limit

			// );
			// return get_data($data, $to_object);
		}

		function direct_all_data($table, $col="*", $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null", $start=0, $limit=-1)
		{
			$this->db->select($col)
					 ->from($table);
			if($limit >= 0) $this->db->limit($limit, $start);
			if($order !== "null") $this->db->order_by($order);
			if($group !== "null") $this->db->group_by($group);
			if($pkjoin !== "null" && $join !== "null") $this->db->join($join, $pkjoin);
			
			$result = $this->db->get();//->result_array();
			if($to_object) return $result->result();
			else return $result->result_array();
			// $condition = "all";
			// $data = array(
			// 	'table' => $table,
			// 	'condition' => 'all',
			// 	'column'	=> $col,
			// 	'order' => $order,
			// 	'join' => $join,
			// 	'pk_join' => $pkjoin,
			// 	'group' => $group,
			// 	'start' => $start,
			// 	'limit'	=> $limit
			// );
			// return get_data($data, $to_object);
		}

		function get_all_data($table, $col="*", $to_object = false, $order="null", $join="null", $pkjoin="null", $group="null", $start=0, $limit=-1)
		{
			$result = $this->rpdb->select($col)
									->from($table)
									->limit($limit, $start)
									->order_by($order)
									->group_by($group)
									->join($join, $pkjoin)
									->get();//->result_array();
			if($to_object) return $result->result();
			else return $result->result_array();
			// $condition = "all";
			// $data = array(
			// 	'table' => $table,
			// 	'condition' => 'all',
			// 	'column'	=> $col,
			// 	'order' => $order,
			// 	'join' => $join,
			// 	'pk_join' => $pkjoin,
			// 	'group' => $group,
			// 	'start' => $start,
			// 	'limit'	=> $limit
			// );
			// return get_data($data, $to_object);
		}

		function __destruct(){ }
	}

 ?>