<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_login extends CI_Model
{
		
	/* Copyright by: VPNKrw */
/* Author: Eryan Fauzan */
/* End of file: m_login.php */
	function cek_user($data)
	{
		$query = $this->db->get_where('tbl_users', $data);
		return $query;
	}
	function get_user($data)
	{
			// $query = $this->db->get_where('tbl_users', $data);
		$query = get_detail_data("users_data", $data);
		
		if ($query != null) {
				// foreach($query as $ceklogin){
			$data['id_user'] = $query['id_user'];
			$data['username'] = $query['username'];
			$data['level'] = $query['level'];
			$data['co_code'] = $query['co_code'];
			$data['kode_perusahaan'] = $query['kode_perusahaan'];
			//$data['foto'] = $query['foto'];
			set_session($data);
				// }
		}


		return $data;
	}
}
?>