<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from big-bang-studio.com/cosmos/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Apr 2018 07:50:56 GMT -->
<head>
    <title>Login Aplikasi</title>
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/logo.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/logo.png" sizes="16x16">
    <link rel="manifest" href="manifest.html">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/vendor.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/cosmos.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/application.min.css">
  </head>
  <body class="authentication-body">
    <div class="container-fluid">
      <div class="authentication-header m-b-30">
        <div class="clearfix">
          <div class="pull-left">
            <a class="authentication-logo" href="index.html">
              <img src="<?php echo base_url();?>assets/logo-bigs.png" alt="" height="65">
            </a>
          </div>
        </div>
      </div>
      <div class="row" style="margin-top:-15px">
        <div class="col-sm-4 col-sm-offset-4">
          <div class="authentication-content m-b-30" style="border:1px solid #009240">
            <h3 class="m-t-0 m-b-30 text-center" Style="font-weight:normal;font-size:13pt">APLIKASI SPKT
			</h3>
			<center><img src="<?php echo base_url();?>assets/all.gif" height="70"style="margin-top:-20px">
			<h5 style="font-weight:normal">SENTRA PELAYANAN<br>KEPOLISIAN TERPADU</h5>
			</center>
            <form method="post" action="<?php echo base_url('login/auth');?>">
			<br>
              <div class="form-group">
                <label for="form-control-1">Username</label>
                <input type="text" class="form-control" name="username" id="form-control-1" placeholder="username">
              </div>
              <div class="form-group">
                <label for="form-control-2">Password</label>
                <input type="password" class="form-control" name="password" id="form-control-2" placeholder="Password">
              </div><br>
              <!--div class="form-group">
                <label class="custom-control custom-control-info custom-checkbox active">
                  <input class="custom-control-input" type="checkbox" name="mode" checked="checked">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-label">Keep me signed in</span>
                </label>
                <a href="http://big-bang-studio.com/pages-reset-password.html" class="text-info pull-right">Forgot password?</a>
              </div-->
              <button type="submit" class="btn btn-success btn-block" style="background-color: #C5901F">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/vendor.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/cosmos.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/application.min.js"></script>

<!-- Mirrored from big-bang-studio.com/cosmos/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Apr 2018 07:50:56 GMT -->
</html>