 <!-- sidebar menu -->
 <?php foreach($queries as $look) { ?>
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
      
              <h3>&nbsp;</h3>
       
              <ul class="nav side-menu">
                <?php 
                /* Updated By Raditya Pratama */
                /* 15 Agustus 2018 01:07 AM */
                foreach ($menu_list as $menu) {
                  $child_menu = get_detail_list("master_menu_app", "parent = '$menu[id]'");
                  $count_child = count($child_menu);
                  $link_parent = ($count_child == 0) ? "href='".base_url($menu['link'])."'" : "";
                  
                  ?>
                  <li>
                      <a <?= $link_parent ?>>
                        <i class="fa <?=$menu['icons']?>"></i> <?=$menu['menu']?>
                        
                        <?php 
                          if($count_child > 0):
                            echo '<span class="fa fa-chevron-down"></span>
                            </a>
                            <ul class="nav child_menu" style="display: none">';
                            foreach ($child_menu as $cm) {
                              # code...
                              $link_child = ($cm['link'] == '' || $cm['link'] == null) ? "javascript:;" : base_url($cm['link']);
                              // $link_child = base_url($link_child);
                              echo "<li>
                                <a href='$link_child'>$cm[menu]</a>
                              </li>";
                            }
                            echo '</ul>';
                          else: echo "</a>";
                          endif; ?>
                  </li>
                  <?php
                } ?>
              <!-- 
                <li>
                  <a href="<?php // echo base_url('users/admin_page');?>"><i class="fa fa-home"></i> Home </a> 
                  
                </li>
                <li>
                  <a><i class="fa fa-cutlery"></i>Marketing<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">        
                    <li><a href="#">Request for Quotation</a></li>
                    <li><a href="#">Insurance Confirmation</a></li>
                    <li><a href="#">Client Confirmation</a></li>
                    <li><a href="#">Marketing Approval</a></li>
                    <li><a href="#">Claims</a></li>
                   </ul>
                </li>
                 <li>
                  <a><i class="fa fa-bar-chart-o"></i>Marketing Report<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="#">Placing Slip</a></li>
                    <li><a href="#">Quotation</a></li>
                    <li><a href="#">Closing Statement</a></li>
                    <li><a href="#">Production</a></li>
                </ul>
                </li>
                <li><a><i class="fa fa-money"></i>Finance<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="#">Payment and Receipt</a></li>
                    <li><a href="#">Setoff</a></li>
                    <li><a href="#">Cancel Payment and Receipt</a></li>
                    <li><a href="#">Cancel Setoff</a></li>
                    <li><a href="#">Adjustment</a></li>
                    <li><a href="#">Finance Approval</a></li>
                  </ul>
                </li>
                <li>
                  <a><i class="fa fa-bar-chart-o"></i>Finance Report<span class="fa fa-chevron-down"></span></a>
                   <ul class="nav child_menu" style="display: none">
                    <li><a href="#">Aging AR/AP</a></li>
                    <li><a href="#">Cash Flow</a></li>
                    <li><a href="#">Statement of Account</a></li>
                    <li><a href="#">Outstanding List</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-money"></i>Accounting<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">        
                    <li><a href="<?php // echo base_url('jurnal');?>">Entry Jurnal</a></li>
                    <li><a href="<?php // echo base_url('coa');?>">Coa</a></li>
                    <li><a href="<?php // echo base_url('coa');?>">Assets Management</a></li>
                    <li><a href="<?php // echo base_url('coa');?>">Taxes</a></li>

                   </ul>
                </li>
                <li>
                  <a><i class="fa fa-bar-chart-o"></i>Accounting Report<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="#">Audit Trail</a></li>
                    <li><a href="#">Balance Sheet</a></li>
                    <li><a href="#">Balance Sheet vs Budget</a></li>
                    <li><a href="#">Trial Balance</a></li>
                    <li><a href="#">Ledger</a></li>
                    <li><a href="#">Profit and Lost</a></li>
                    <li><a href="#">Faktur Pajake</a></li>
                    <li><a href="#">OJK Report</a></li>
                  </ul>
                </li>

                 <li><a><i class="fa fa-money"></i>Static Data<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">        
                    <li><a href="<?php echo base_url('begining');?>">Begining Balance</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Budget & Target</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Bussines Rule</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Chart of Account</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Client Information</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Exchange Rate</a></li>
                    <li><a href="<?php echo base_url('coa');?>">Error of Menu</a></li>

                   </ul>
                </li>
              </ul>

               -->
               </ul>
            </div>
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
      
         
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
    <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
             <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<?php echo base_url('./assets/image/foto_user/admin_icon.png')?>" alt=""><?php echo $look->nama_user ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                    <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                 </li>
                </ul>
              </li>            

            </ul>
          </nav>
        </div>

      </div>
     <?php } ?>
      <!-- /top navigation -->