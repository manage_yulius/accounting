<!DOCTYPE html>
<html lang="en">
	<head>
	<?php $this->load->view('template/css');?>
	</head>
	<body class="layout layout-header-fixed layout-left-sidebar-fixed nav-md">
    <?php $this->load->view($header);?>
      <?php $this->load->view($menu);?>
      <?php $this->load->view($content);?>
      <?php $this->load->view('template/js');?>
      <div id="modal-container"></div>
  </body>
</html>