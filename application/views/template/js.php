      <!-- footer content -->

        <footer>
          <div class="copyright-info">
            <p class="pull-right"><a href="https://ligasys.com">LigaSys</a> - insurance solutions @ 2018
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
    
  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<?php echo base_url();?>assets/gentelella/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/gentelella/js/input_mask/jquery.inputmask.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/gentelella/js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/gentelella/js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="<?php echo base_url();?>assets/gentelella/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="<?php echo base_url();?>assets/gentelella/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="<?php echo base_url();?>assets/gentelella/js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/gentelella/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/gentelella/js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="<?php echo base_url();?>assets/gentelella/js/chartjs/chart.min.js"></script>

  <!-- skycons -->
  <script src="<?php echo base_url();?>assets/gentelella/js/skycons/skycons.min.js"></script>
  

  <!-- dashbord linegraph -->
 
  <!-- /dashbord linegraph -->
  <script src="<?php echo base_url();?>assets/gentelella/js/custom.js"></script>
 
  <!-- select2 -->
  <script src="<?php echo base_url();?>assets/gentelella/js/select/select2.full.js"></script>
 
  <!-- Autocomplete -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/gentelella/js/autocomplete/countries.js"></script>
  <script src="<?php echo base_url();?>assets/gentelella/js/autocomplete/jquery.autocomplete.js"></script>
  <!-- Datatables-->
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/jszip.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/pdfmake.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/vfs_fonts.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/buttons.html5.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/buttons.print.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.scroller.min.js"></script>
        <script src="<?php echo base_url();?>assets/gentelella/js/datatables/dataTables.select.min.js"></script>

        <script src="<?php echo base_url()?>assets/js/jquery-number/jquery.number.js"></script>
  <!-- pace -->
  <script src="<?php echo base_url();?>assets/gentelella/js/pace/pace.min.js"></script>
  <script type="text/javascript">
    $(function() {
      'use strict';
      var countriesArray = $.map(countries, function(value, key) {
        return {
          value: value,
          data: key
        };
      });
      // Initialize autocomplete with custom appendTo:
      $('#autocomplete-custom-append').autocomplete({
        lookup: countriesArray,
        appendTo: '#autocomplete-container'
      });
    });
  </script>
  

		<script type="text/javascript">
    var table1 = null;
          $(document).ready(function() {
            $('#datatable_client').DataTable();
            table1 = $('#datatable').DataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "<?php echo base_url();?>assets/gentelella/js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
            <?php if(isset($need_onClick_table)){
              ?>
              function row_click_event(){
                // table1.clear();
                table1.destroy();

                table1 = $('#datatable').DataTable({
                    "columnDefs": [
                        {
                            "targets": [ 9,12 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
                    select: {
                      style: 'single'
                    },
                    ajax: '<?= base_url() ?>coa/data_coa',
                    "columns": [
                        { "data": "coa_code" },
                        { "data": "is_header" },
                        { "data": "roll_up_id" },
                        { "data": "normal" },
                        { "data": "description" },
                        { "data": "audit_trail" },
                        { "data": "balance_sheet" },
                        { "data": "cash_flow" },
                        { "data": "profit_loss" },
                        { "data": "classification_id" },
                        { "data": "classification" },
                        { "data": "status" },
                        { "data": "id" }
                    ]
                });
                $('#datatable tbody').on('click', 'tr', function () {
                    
                    disabledForm(false, 'edit');
                    if ( $(this).hasClass('selected') ) {
                        $(this).removeClass('selected');
                        activateForm('all', false);
                    }
                    else {
                        table1.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        var data = table1.row( this ).data();

                        fillForm(data);
                    }
                } );
              }
              row_click_event();
              <?php
            } ?>
          });

          //TableManageButtons.init();
        </script>

  <?php if ($this->session->flashdata('input_sukses')) {?>   
			<script type="text/javascript">
			$(function(){
				swal("Success", "Berhasil Disimpan ke Database", "success")
				});
			</script>
		<?php }?>
		<?php if ($this->session->flashdata('input_gagal')) {?>   
			<script type="text/javascript">
			$(function(){
				swal("Failed", "Gagal Menyimpan ke Database", "error")
				});
			</script>
		<?php }?>
		<?php if ($this->session->flashdata('update_sukses')) {?>   
			<script type="text/javascript">
			$(function(){
				swal("Information", "Data Berhasil di Update", "info")
				});
			</script>
		<?php }?>
		<?php if ($this->session->flashdata('update_gagal')) {?>   
			<script type="text/javascript">
			$(function(){
				swal("Failed", "Data Gagal di Update", "error")
				});
			</script>
		<?php }?>
		
		 <?php if ($this->session->flashdata('delete_sukses')) {?>   
			<script type="text/javascript">
			$(function(){
				swal("Deleted", "Data Berhasil di Hapus", "error")
				});
			</script>
		<?php }?>
		
		<script>		
        jQuery(document).ready(function($){
            $('.hapus').on('click',function(){
               
				var url = $(this).attr('href');
				swal({
					title: 'Hapus Data dari Database?',
					
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus Data',
          cancelButtonText: "Batal",
				}).then(function () {
					window.location.href = url});
                
                return false;
            });
        });
    </script>
	<script>		
        jQuery(document).ready(function($){
            $('.batal').on('click',function(){
               
				var url = $(this).attr('href');
				swal({
					title: 'Batal ?',
					text: "Segala Input dan Perubahan Tidak akan Tersimpan",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'OK'
					}).then(function () {
					window.location.href = url});
                
                return false;
            });
        });
    </script>	
	<script>
    $(document).ready(function() {
      $(":input").inputmask();
    });
  </script>

		
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/highcharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/modules/exporting.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/modules/data.js'); ?>"></script>

	

 <!-- select2 -->
  <script>
  function randomString(length) {
                // var strlength = (typeof length === 'undefined') ? 25:length;
                let text = "";
                let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#%^&*";

                for (var i = 0; i < length; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                return text;
            }

            function encryptString(str, length) {
                var strlength = (typeof length === 'undefined') ? 10 : length;
                let plain = this.randomString(strlength) + "$" + btoa(str) + "$" + this.randomString(strlength);
                let base64 = btoa(plain);
                return base64;
            }
    $(document).ready(function() {
      $(".select2_single").select2({
        placeholder: "Pilih Kategori",
        allowClear: true
      });
      $(".select2_group").select2({});
      $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
      });
    });
  </script>
  <!-- /select2 -->