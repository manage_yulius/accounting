<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="modal-form">
                
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"><?=$modal_title?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php $this->load->view($form); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary modal-btn" data-dismiss="modal">Close</button>
                    <button type="button" id="save-btn" class="btn btn-primary modal-btn">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">
    $("#mymodal").modal({
        show: true, backdrop: 'static'
    }).on('hidden.bs.modal', function(event) {
        $("#modal-container").html("");
        // end_block();
    });
</script>
<?php if (isset($js_modal)) {
    $this->load->view($js_modal);
} ?>