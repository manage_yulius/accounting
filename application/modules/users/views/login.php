
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>:: Halaman Login ::</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Login Page Accounting Management System" name="description" />
        <meta content="KodeKoding ft LigaSys" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"/>
        <link href="<?=ASSETS_URL?>metronic/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="<?=ASSETS_URL?>metronic/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"/>
        <link href="<?=ASSETS_URL?>metronic/css/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?= ASSETS_URL ?>gentelella/css/animate.min.css" rel="stylesheet"/>

        <!-- <link href="<?=ASSETS_URL?>metronic/css/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"/> -->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=ASSETS_URL?>metronic/css/components.min.css" rel="stylesheet" id="style_components"/>
        <link href="<?=ASSETS_URL?>metronic/css/plugins.min.css" rel="stylesheet"/>
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?=ASSETS_URL?>metronic/css/login.min.css" rel="stylesheet"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?=ASSETS_URL?>image/logo.png" /> 
        <style>
        .login{
            background-color: #fdfdfd!important;
        }
        </style>
        <link href="<?= ASSETS_URL ?>sweetalert/dist/sweetalert2.min.css" rel="stylesheet"/>
        <script src="<?= ASSETS_URL ?>metronic/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_URL ?>metronic/css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_URL ?>sweetalert/dist/sweetalert2.min.js" type="text/javascript"></script>

        </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<?=ASSETS_URL?>image/logo.png" alt="" /><span style="font-weight: 700; font-size: 16pt">LIGASYS</span></a>
        </div>
        <!-- END LOGO -->
        <!-- <div class="copyright"> <b></b> </div> -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="border: 1px solid black;border-top: 5px solid red">
            <!-- BEGIN LOGIN FORM -->
            <form id="login-frm">
                <h3 class="form-title" style="color: black; font-size: 16pt">Sign In to Your Account</h3>
                <div class="alert alert-danger display-hide" id="error-container">
                    <button class="close" data-close="alert"></button>
                    <span id="pesan-error"> Enter any username and password. </span>
                    
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" id="username" required placeholder="Username" onkeypress="runScript(event)" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" required placeholder="Password" name="password" id="password" onkeypress="runScript(event)"/> </div>
                <div class="form-actions">
                    
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Remember
                        <span></span>
                    </label>
                    <!-- <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a> -->
                    <span class='pull-right'>
                        <a id="login-btn" class="btn green uppercase">Login</a>
                    </span>
                </div>
                <!-- <div class="login-options">
                    <h4>Or login with</h4>
                    <ul class="social-icons">
                        <li>
                            <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                        </li>
                    </ul>
                </div> -->
                <div class="create-account" style="background-color: #FFFFFF">
                    <!-- <p> -->
                        <a onclick="showModal('register')" class="btn grey uppercase" style="border: 1px solid black">Register Now</a>
                        <a style="border: 1px solid black" href="javascript:;" id="register-btn" class="btn grey uppercase">Forgot Password</a>
                    <!-- </p> -->
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <!-- <form class="forget-form" action="index.html" method="post">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form> -->
            <!-- END FORGOT PASSWORD FORM -->
            
        </div>
            <div id="modal-container"></div>

        <!-- <div class="copyright"> Don't have account yet ? <b>Sign Up</b> </div> -->
        <!--[if lt IE 9]>
<script src="<?=ASSETS_URL?>metronic/css/respond.min.js"></script>
<script src="<?=ASSETS_URL?>metronic/css/excanvas.min.js"></script> 
<script src="<?=ASSETS_URL?>metronic/css/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        
        <script src="<?=ASSETS_URL?>metronic/js/js.cookie.min.js" type="text/javascript"></script>
        <!-- <script src="<?=ASSETS_URL?>metronic/css/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> -->
        <script src="<?=ASSETS_URL?>metronic/js/jquery.blockui.min.js" type="text/javascript"></script>
        <!-- <script src="<?=ASSETS_URL?>metronic/css/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> -->
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=ASSETS_URL?>metronic/js/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=ASSETS_URL?>metronic/js/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=ASSETS_URL?>metronic/js/login.min.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

            $("#login-btn").click(loginCheck);

            // $("#login-frm").submit(loginCheck);

            function runScript(e) {
                // e.preventDefault();
                if (e.keyCode == 13) {
                    e.preventDefault();
                    loginCheck();
                }
                return false;
            }
            
            function randomString(length) {
                // var strlength = (typeof length === 'undefined') ? 25:length;
                let text = "";
                let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#%^&*";

                for (var i = 0; i < length; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                return text;
            }

            function encryptString(str, length) {
                var strlength = (typeof length === 'undefined') ? 10 : length;
                let plain = this.randomString(strlength) + "$" + btoa(str) + "$" + this.randomString(strlength);
                let base64 = btoa(plain);
                return base64;
            }

            function showModal(type) {
                $.get('<?= BASE_URL ?>users/show_modal/'+type, function (response) { 
                    $("#modal-container").html(response);
                 });
            }

            function getFormData() {
                var dt = $("#login-frm").serializeArray();
                // console.log(dt);
                $.each(dt, function (key, val) {
                    val.value = encryptString(val.value);
                })
                return dt;
            }

            function loginCheck() {
                var dt = getFormData();
                // console.log(dt);
                $.post('<?=BASE_URL?>auth/process', dt, function (response, x,h) { 
                    
                    // var resp = $.parseJSON(response); //if response type is JSON
                    // console.log(response);
                    // alert(resp.status_code);
                    if(response.status_code == 0){
                        $("#pesan-error").html(response.message);
                            $("#login-frm").trigger("reset");
                        $("#error-container").show();
                            $("#username").focus();
                        setTimeout(() => {
                            $("#error-container").hide('fast');
                        }, 2000);

                        return false;
                    }
                    else {
                        window.location = '<?=BASE_URL?>'+response.page;
                    }
                 }, 'json');
            }
        </script>
    </body>

</html>