<div class="alert alert-danger" id="error-message">
    test
</div>

<div class="form-group">
    <label for="firstName" class="" style="width: 30%">First Name <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input type="text" required name="first_name-valid:alpha" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Last Name <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input type="text" required name="last_name-valid:alpha" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Company <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input type="text" required name="company-valid:alpha" style="width: 65%; display: inline" class="form-control">
    <!-- <select required name="id_perusahaan-alt:Company" id="id_perusahaan" style="width: 65%; display: inline" class="form-control">
        <option value="">- pilih -</option>
        <?php /* foreach ($list_company as $perusahaan) {
            echo "<option value='$perusahaan[no_urut]'>$perusahaan[nama_perusahaan] - $perusahaan[kode_perusahaan]</option>";
        } */ ?>
    </select> -->
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Email <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input required type="email" name="email-valid:valid_email" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Telephone <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input required type="text" name="telp-valid:numeric;min_length=8;max_length=15;-alt:telephone" maxlength="15" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Username <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input required type="text" name="username" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Password <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input required type="password" name="password-valid:min_length=6;-alt:Password" style="width: 65%; display: inline" class="form-control">
</div>
<div class="form-group">
    <label for="firstName" class="" style="width: 30%">Confirm Password <font color="red">*</font></label>
    <label style="width: 5%; display: inline">:</label>
    <input required type="password" name="conf_password-alt:Confirm Password-valid:matches=password;" style="width: 65%; display: inline" class="form-control">
</div>