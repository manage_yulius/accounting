<script type="text/javascript">
    // alert('im js modal');
    $("#error-message").hide();
    $("#save-btn").click(function () {
        $(".modal-btn").prop("disabled", true);
        var formData = $("#modal-form").serialize();
        $.post('<?= BASE_URL.$link ?>', formData, function (response) {
            var dt = $.parseJSON(response);
            console.log(dt);
            $(".modal-btn").prop("disabled", false);
            if(dt.status == -1){
                $("#error-message").html(dt.message).show('fast');
                setTimeout(() => {
                    $("#error-message").html('').hide('fast');
                }, 5000);
                
            }
        });
    });
</script>