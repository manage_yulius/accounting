<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_login');
        $this->load->model('m_admin');
        $this->load->model('m_keuntungan');
        $this->load->model('m_hpp');
        $this->load->helper('url');
        $this->load->helper('download');
    }

    function index()
    {
        # code...
        // echo "kosong";
    }

    function check_connection()
    {
        $koneksi = mysqli_connect('149.56.109.138', 'admin', 'accounting@2018', 'insurance_db');
        if (!$koneksi) exit("Koneksi Gagal");
        echo "Koneksi Berhasil";
    }

    public function login()
    {
        $this->load->view('login');
        
    }

    public function proses_login()
    {
        check_ajax_process();
        $uname = $this->input->post('username', true);
        $pass = $this->input->post('password', true);
        
        $data = (array(
            'username' => decode_str($uname),
            'password' => sha1(decode_str($this->input->post('password')))
        ));
        $data = $this->m_login->get_user($data);
        // $code_message = 0;
        $result['status_code'] = 1;
        $result['message'] = 'Welcome';
        if ($data['level'] == 'Admin') {
            $credentials = array('usr_id' => $data['id_user'], 'login_number'=> '1');
            $this->session->set_userdata($credentials);
            // redirect('users/admin_page');
            $result['page'] = 'users/admin_page';
        } else if ($data['level'] == 'Kasir') {
            // redirect('kasir');
            $result['page'] = 'kasir';

        } else {
            $result['status_code'] = 0;
            $result['message'] = get_message('login_failed');
            // redirect('auth', 'refresh');
        }
        echo json_encode($result);
    }

    public function admin_page(){
        $users = 'User';
        $admin = 'Admin';
        $id_user = $this->session->userdata('id_user');
        $tanggal = date("Y-m-d");
        /*foreach($this->m_admin->omzet_chart()->result_array() as $row){
            $data['grafik'][]=(float)$row['Januari'];
            $data['grafik'][]=(float)$row['Februari'];
            $data['grafik'][]=(float)$row['Maret'];
            $data['grafik'][]=(float)$row['April'];
            $data['grafik'][]=(float)$row['Mei'];
            $data['grafik'][]=(float)$row['Juni'];
            $data['grafik'][]=(float)$row['Juli'];
            $data['grafik'][]=(float)$row['Agustus'];
            $data['grafik'][]=(float)$row['September'];
            $data['grafik'][]=(float)$row['Oktober'];
            $data['grafik'][]=(float)$row['November'];
            $data['grafik'][]=(float)$row['Desember'];
        }
        
        foreach($this->m_admin->modal_chart()->result_array() as $row){
            $data['grafik1'][]=(float)$row['Januari'];
            $data['grafik1'][]=(float)$row['Februari'];
            $data['grafik1'][]=(float)$row['Maret'];
            $data['grafik1'][]=(float)$row['April'];
            $data['grafik1'][]=(float)$row['Mei'];
            $data['grafik1'][]=(float)$row['Juni'];
            $data['grafik1'][]=(float)$row['Juli'];
            $data['grafik1'][]=(float)$row['Agustus'];
            $data['grafik1'][]=(float)$row['September'];
            $data['grafik1'][]=(float)$row['Oktober'];
            $data['grafik1'][]=(float)$row['November'];
            $data['grafik1'][]=(float)$row['Desember'];
        }
        
        foreach($this->m_admin->keuntungan_chart()->result_array() as $row){
            $data['grafik2'][]=(float)$row['Januari'];
            $data['grafik2'][]=(float)$row['Februari'];
            $data['grafik2'][]=(float)$row['Maret'];
            $data['grafik2'][]=(float)$row['April'];
            $data['grafik2'][]=(float)$row['Mei'];
            $data['grafik2'][]=(float)$row['Juni'];
            $data['grafik2'][]=(float)$row['Juli'];
            $data['grafik2'][]=(float)$row['Agustus'];
            $data['grafik2'][]=(float)$row['September'];
            $data['grafik2'][]=(float)$row['Oktober'];
            $data['grafik2'][]=(float)$row['November'];
            $data['grafik2'][]=(float)$row['Desember'];
        }*/
            
        /*$data['total_users']= $this->m_admin->total_users();
        $data['admin']=$this->m_admin->admin($admin);
        $data['users']=$this->m_admin->users($users);
        $data['posting']=$this->m_admin->total_posting();*/
        $data['queries']= $this->m_admin->get_id_user($id_user)->result();
        $data['username']=$this->session->userdata('username');
        $data['level']=$this->session->userdata('level');
        $data['foto']=$this->session->userdata('foto');
        /*$data['total_modal_harian']=$this->m_hpp->total_modal_harian($tanggal);
        $data['total_omzet_harian']=$this->m_hpp->total_omzet_harian($tanggal);
        $data['total_transaksi']=$this->m_hpp->total_transaksi($tanggal);
        
        $data['tot_penjualan']  = $this->m_hpp->tot_penjualan($tanggal);
        $data['tot_biaya_lain'] = $this->m_hpp->tot_biaya_lain($tanggal);*/
        $this->page->view('dashboard',$data);
    }

    function logout()
    {
        # code...
        session_unset();
        session_destroy();
        redirect('auth');
    }

    function __destruct() { }
}
?>