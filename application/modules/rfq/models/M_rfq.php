<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');
class M_rfq extends CI_Model{
	
	public $CategoryID;         //:int
    public $ParentCategoryID;     //:int
    public $Category;             //:str
    public $Active;             //:bool
    
    public $select_array;         //:array

    public function __construct() {
        
        parent::__construct();
        
        $this->CategoryID = 0;
        $this->ParentCategoryID = 1;
        $this->Category = '';
        $this->Active = 1;
        
    }

    function list_data() {
        $this->db->select('tbl_rfq_general.*, tbl_client.name');
        $this->db->from("tbl_rfq_general");
        $this->db->join('tbl_client','tbl_client.id = tbl_rfq_general.client');
        //$this->db->group_by("accounting_accounts.name");
        return $this->db->get();
    }

    function list_jurnal_by_id($id) {
        return $query = $this->db->query("SELECT * FROM accounting_journal where id='$id'");
    }

    function list_data_by_id($id) {
        $this->db->select('table_balance.*');
        $this->db->where(array('ID'=>$id));
        $this->db->from("table_balance");
        return $this->db->get();
    }
    function get_list_account(){		
		$this->db->order_by('nomor','asc');
		$accounts = $this->db->get('accounting_accounts');
		$data = array();
		$data[''] = '-Pilih Akun-';
		
		foreach($accounts->result_array() as $row):
			$data[$row['id']] = $row['nomor'].'-'.$row['name'];
		endforeach;
		return $data;
	}

    function save_data(){
        $id = $this->input->post('id');
        if($id != 0):
             $data = array(
                'THN'=>$this->input->post('tahun'),
                'COAID'=>$this->input->post('COAID'), 
                'OFFICEID'=>$this->input->post('office'),
                'DEBIT'=>$this->input->post('DEBIT'), 
                'CREDIT'=>$this->input->post('CREDIT'), 
                );
            $this->db->where('ID',$id);
            $this->db->update('tbl_rfq_general', $data);
            return $id;
        else:
            $data = array(
                'request_id'=>$this->input->post('request_id'),
                'type_cover'=>$this->input->post('type_cover'), 
                'channel'=>$this->input->post('channel'),
                'sales'=>$this->input->post('sales'), 
                'client'=>$this->input->post('client'), 
                'insured_name'=>$this->input->post('insured_name'), 
                'start_periode'=>$this->input->post('start_periode'), 
                'end_periode'=>$this->input->post('end_periode'), 
                'interest_insured'=>$this->input->post('interest_insured'), 
                'risk_location'=>$this->input->post('risk_location'), 
                'currency'=>$this->input->post('currency'), 
                'rate'=>$this->input->post('rate'), 
                'sum_insured'=>$this->input->post('sum_insured'), 
                'payment'=>$this->input->post('payment'), 
                'ppw'=>$this->input->post('ppw'), 
                'policy_insurance'=>$this->input->post('policy_insurance'), 
                'remark'=>$this->input->post('remark'), 
                );
            $this->db->insert('tbl_rfq_general', $data);
            $id_last = $this->db->insert_id();//

            return $this->db->insert_id();
        endif;
    }

  	
    function delete_data($id){
        $this->db->where('ID',$id);
        $this->db->delete('tbl_rfq_general');
        
        return true;
    }
}
?>