<!-- <div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>
<div class="container"><h2>Example tab 2 (using standard nav-tabs)</h2></div> -->

<div class="row">
    <div class="col-xs-6">
        <div class="row">
           <div class="col-xs-6 form-group">
              <label>Type of Cover</label>
               <select name="type_cover" class="form-control" required>
                <?php 
                
                foreach ($type_cover as $key => $value) { 
                  if($jurnal->type_cover==$value['id']){
                    $sel = "selected";
                  }else{
                    $sel = "";    
                  }
                  ?>
                  <option value="<?php echo $value['id']?>" <?php echo $sel?>>-<?php echo $value['description']?>-</option>
                <?php }?>
             </select>
          </div>     
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Channel</label>
                <input class="form-control" type="text" required name="channel" value="<?php echo(isset($jurnal->channel)?$jurnal->channel:"")?>" />
                 <input class="form-control" type="hidden" name="id" value="<?php echo(isset($jurnal->ID)?$jurnal->ID:"")?>" />
                 <input class="form-control" type="hidden" name="request_id" value="<?php echo(isset($jurnal->request_id)?$request_idrnal->ID:time())?>" />
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Sales/Officer</label>
                <input required class="form-control" type="text" name="sales" value="<?php echo(isset($jurnal->sales)?$jurnal->sales:"")?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Client</label>
                <select name="client" class="form-control" required>
                <?php 
                
                foreach ($client as $key => $value) { 
                  if($jurnal->client==$value['id']){
                    $sel = "selected";
                  }else{
                    $sel = "";    
                  }
                  ?>
                  <option value="<?php echo $value['id']?>" <?php echo $sel?>>-<?php echo $value['name']?>-</option>
                <?php }?>
             </select>
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Address</label>
                <input required class="form-control" type="text" name="address" value="<?php echo(isset($jurnal->address)?$jurnal->address:"")?>" />
            </div>
        </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Insured Name</label>
                <input class="form-control" type="text" required name="insured_name" value="<?php echo(isset($jurnal->insured_name)?$jurnal->insured_name:"")?>" />
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Periode</label>
                <input required class="form-control" type="text" name="periode" value="<?php echo(isset($jurnal->periode)?$jurnal->periode:"")?>" />
            </div>
        </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Inception Date</label>
                <input class="form-control" type="text" required name="inception_date" value="<?php echo(isset($jurnal->inception_date)?$jurnal->inception_date:"")?>" />
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Insterest Insured</label>
                <input required class="form-control" type="text" name="interest_insured" value="<?php echo(isset($jurnal->interest_insured)?$jurnal->interest_insured:"")?>" />
            </div>
        </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Risk Location</label>
                <input class="form-control" type="text" required name="risk_location" value="<?php echo(isset($jurnal->risk_location)?$jurnal->risk_location:"")?>" />
            </div>
           
        </div>
         <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Currency</label>
                <input class="form-control" type="text" required name="currency" value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>" />
                
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Rate</label>
                <input required class="form-control" type="text" name="rate" value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Sum Insured</label>
                <input class="form-control" type="text" required name="sum_insured" value="<?php echo(isset($jurnal->sum_insured)?$jurnal->sum_insured:"")?>" />
                
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Payment</label>
                <input required class="form-control" type="text" name="payment" value="<?php echo(isset($jurnal->payment)?$jurnal->payment:"")?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">PPW</label>
                <input class="form-control" type="text" required name="ppw" value="<?php echo(isset($jurnal->ppw)?$jurnal->ppw:"")?>" />
            </div>
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Policy Issuance</label>
                <input required class="form-control" type="text" name="policy_insurance" value="<?php echo(isset($jurnal->policy_insurance)?$jurnal->policy_insurance:"")?>" />
            </div>
        </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12">
            <label class="col-xs-12">Remark</label>
                <input class="form-control" type="text" required name="remark" value="<?php echo(isset($jurnal->remark)?$jurnal->remark:"")?>" />         
            </div>
           
        </div>
    </div>
   
    
    
</div>
