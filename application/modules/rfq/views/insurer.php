<!-- <div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>
<div class="container"><h2>Example tab 2 (using standard nav-tabs)</h2></div> -->

<div class="row">
    <div class="col-xs-12">
        <div class="row">
           <div class="col-xs-6 form-group">
              <label>Insurer</label>
               <select name="client" class="form-control" required>
                <?php 
                
                foreach ($insurer as $key => $value) { 
                  if($jurnal->insurer==$value['id']){
                    $sel = "selected";
                  }else{
                    $sel = "";    
                  }
                  ?>
                  <option value="<?php echo $value['id']?>" <?php echo $sel?>>-<?php echo $value['name']?>-</option>
                <?php }?>
             </select>
          </div>     
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
            <label class="col-xs-12">Type</label>
                <input class="form-control" type="text" required name="office" value="<?php echo(isset($jurnal->OFFICEID)?$jurnal->OFFICEID:"")?>" />
                 <input class="form-control" type="hidden" name="id" value="<?php echo(isset($jurnal->ID)?$jurnal->ID:"")?>" />
            </div>
            <div class="col-xs-6">
              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                  <label class="col-xs-12">Share</label>
                      <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                  <label class="col-xs-12">Comm. Fee</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
            <div class="x_content">
            <table id="datatable-2" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Insurance</th>                        
                    <th>PIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Last Transaction</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            </div>
        </div>
        <div class="row">
             <div class="col-xs-12">
              <div class="row">
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Premium</label>
                      <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Policy Cost</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Stamd Duty</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
              </div>
          </div>
           <div class="col-xs-12">
              <div class="row">
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Admin Fee</label>
                      <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Bank Fee</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Stamd Duty</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
              </div>
          </div>
          <div class="col-xs-12">
              <div class="row">
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Others 1</label>
                      <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Others 2</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
                 
              </div>
          </div>
          <div class="col-xs-12">
              <div class="row">
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">PPW 1</label>
                      <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                  <label class="col-xs-12">Policy Insuance</label>
                      <input class="form-control" type="text" name="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"")?>" />
                  </div>
                 
              </div>
          </div>
        </div>
    </div>
   
    
    
</div>
