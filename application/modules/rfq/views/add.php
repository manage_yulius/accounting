<!-- <div class="container"><h1>Bootstrap  tab panel example (using nav-pills)  </h1></div>
<div class="container"><h2>Example tab 2 (using standard nav-tabs)</h2></div> -->
<form id="demo-form2" data-parsley-validate method="post" action="<?php echo base_url('rfq/simpan_data');?>" enctype="multipart/form-data" class="form-horizontal form-label-left">
          <?php validation_errors();?>
<div id="exTab2" class="container"> 
<ul class="nav nav-tabs">
      <li class="active">
        <a  href="#1" data-toggle="tab">General Information</a>
      </li>
      <li><a href="#2" data-toggle="tab">Insurer</a>
      </li>
      <li><a href="#3" data-toggle="tab">Comission Out</a>
      </li>
      <li><a href="#4" data-toggle="tab">Particulars</a></li>
      <li><a href="#5" data-toggle="tab">Check List</a></li>
    </ul>

      <div class="tab-content ">
        <div class="tab-pane active" id="1">
          <!-- <h3>List General Information</h3> -->
          <?php $this->load->view('general')?>
        </div>
        <div class="tab-pane" id="2">
          <!-- <h3>Notice the gap between the content and tab after applying a background color</h3> -->
          <?php $this->load->view('insurer')?>
        </div>
        <div class="tab-pane" id="3">
          <h3>add clearfix to tab-content (see the css)</h3>
        </div>
        <div class="tab-pane" id="4">
          <h3>Particular</h3>
        </div>
        <div class="tab-pane" id="5">
          <h3>Check List</h3>
        </div>
      </div>
       <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
            <?php echo form_submit(array('value'=>'Simpan','name'=>'Simpan','class'=>'btn btn-primary'));?>
            <?php if($aksi=="edit"):?>
            <a href="<?php echo base_url('begining');?>" class="btn btn-danger batal" role="button">Cancel</a>
          <?php endif;?>
          </div>

        </div>
  </div>
  </form>