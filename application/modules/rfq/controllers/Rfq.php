<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rfq extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_rfq');
	}

	public function index()
	{
		$data['aksi'] = "add";
		$data['row'] = $this->m_rfq->list_data();
		$accounts = $this->m_rfq->get_list_account();
        $data['accounts'] = $accounts;
        $data['type_cover'] = get_all_data("tbl_type_cover");
        $data['client'] = get_all_data("tbl_client");
        $data['insurer'] = get_all_data("tbl_insurer");
		//$this->load->view('main', $data);
		$this->page->view('main',$data);
	}

	function tambah_data(){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        $data['aksi'] = "add";
        $accounts = $this->m_rfq->get_list_account();
        $data['CategoryID'] = "7";
        $data['category_options'] = $this->m_rfq->getAllCategoriesSelector('');
		$data['accounts'] = $accounts;
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function edit($id){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        $accounts = $this->m_rfq->get_list_account();
        $data['CategoryID'] =       "7";
        $data['category_options'] = $this->m_rfq->getAllCategoriesSelector('');
		$data['accounts'] = $accounts;
		$data['aksi'] = "edit";
		$data['jurnal'] = $this->m_rfq->list_data_by_id($id)->row();
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function detail($id){
		$data['jurnal'] = $this->m_rfq->list_jurnal_by_id($id)->row();
		$data['detail'] = $this->m_rfq->list_jurnal_detail($id);
		$this->page->view('view',$data);
	}

	function simpan_data(){
		$this->db->trans_begin();
		if($this->m_rfq->save_data()):
			log_message('info', 'Entry Beginning Balance');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};
		endif;
			$this->session->set_flashdata('Input Beginning Balance Sukses','pesan');
			redirect('Begining');
	}	

	function delete($id){
		$this->db->trans_begin();
		if($this->m_rfq->delete_data($id)):
			$this->activity_log->log('Delete Beginning Balance');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};
			echo(json_encode(array('status'=>'sukses','msg'=>'Data berhasil dihapus.')));
		else:
			echo(json_encode(array('status'=>'sukses','msg'=>'Data gagal dihapus.')));
		endif;
		  redirect('Begining');
	}

	function __destruct(){ }
}