
   <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        
        <!-- /top tiles -->

        <br />

        <div class="row">
		 			
		<?php echo $this->session->flashdata('pesan');?>
          <div class="col-md-12 col-sm-12 col-xs-12">
		  
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>CHART OF ACCOUNT</strong></h2>
				  
                  <div class="clearfix"></div>
                </div>
				<!-- <a type="button" class="btn btn-round btn-success" href="<?php //echo base_url('Coa/tambah_data');?>"><i class="fa fa-plus-square">&nbsp </i>Tambah COA</a> -->
        <font color='red'>*Click row of table to edit the data</font>
                <div class="x_content">
                  <table id="datatable" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                      <tr>
                        <!-- <th>#</th> -->
                        <th>COA Code</th>                        
                        <th>Header/Detail</th>
                        <!-- <th>Roll Up ID</th> hidden -->
                        <th>Roll Up</th>
                        <th>Normal</th>
                        <th>Description</th>
                        <th>Audit Trail</th>
                        <th>Balance Sheet</th>
                        <th>Cash Flow</th>
                        <th>Profit Loss</th>
                        <th>Classification ID</th> <!-- hidden -->
                        <th>Classification</th>
                        <th>Status</th>
                        <th>ID</th> <!-- hidden -->
                        
                      </tr>
                    </thead>


                     <!-- <tbody> -->
					
					<?php $no = 1; ?>
							<?php /* foreach ($row as $look) {
    ?>
						<tr> 
							<td><?= $no++; ?></td>
							<td><?= $look['coa_code']; ?></td>							
              <td><?=$look['is_header'] == 1 ? "Header" :"Detail"; ?></td>
							<td><?= $look['roll_up_id']; ?></td>							
							<td><?= $look['roll_up']; ?></td>							
							<td><?= $look['normal']; ?></td>							
							<td><?= $look['description']; ?></td>							
							<td><?= $look['audit_trail'] == 1 ? "Y":"N"; ?></td>							
							<td><?= $look['balance_sheet'] == 1 ? "Y":"N"; ?></td>							
							<td><?= $look['cash_flow'] == 1 ? "Y":"N"; ?></td>							
							<td><?= $look['profit_loss'] == 1 ? "Y":"N"; ?></td>							
							<td><?= $look['classification_id']; ?></td>							
							<td><?= $look['classification']; ?></td>							
							<td><?= $look['status']==1? "Active" : "Non Active"; ?></td>							
							<td><?= $look['id'] ?></td>							
              
						</tr>
							
							<?php
} */ ?>
							
						
                      
                    <!-- </tbody> -->
                  </table>
                
                <hr>
                <div class="alert alert-danger" id='message-container' style="display:none">
                  tes
                </div>
                <form id="form-coa">
                  <div class="form-group">
                    <label for="firstName" class="" style="width: 10%">COA Code <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <input required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="nomor-alt:coa_code" id="coa_code" style="width: 15%; display: inline" class="form-control">
                    <label style="width: 5%; display: inline">&nbsp;</label>
                    <label for="firstName" class="" style="width: 10%">Description <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <input required type="text" name="name-alt:description" id="description" style="width: 30%; display: inline" class="form-control">
                    <label style="width: 5%; display: inline">&nbsp;</label>
                    <label for="firstName" class="" style="width: 10%">Header <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <select name="is_header-alt:header" style="width: 15%; display: inline"  id="header" class="form-control">
                      <!-- <option value="">-pilih-</option> -->
                      <option value="1">Header</option>
                      <option value="0">Detail</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="firstName" class="" style="width: 10%">Normal <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <select name="estimates-alt:Normal" id="normal" style="width: 15%; display: inline" class="form-control">
                      <!-- <option value="">-pilih-</option> -->
                      <option>DEBIT</option>
                      <option>CREDIT</option>
                      
                    </select>
                    <label style="width: 5%; display: inline">&nbsp;</label>
                    <label for="firstName" class="" style="width: 10%">Roll Up <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <select name="parent_id-alt:roll_up" id="roll_up" style="width: 60%; display: inline" class="form-control">
                      <!-- <option value="">-pilih-</option> -->
                      <?php foreach ($list_parent as $kp => $parent) {
                        echo "<option value='$parent[nomor]'>$parent[nomor] - $parent[name]</option>";
                      } ?>
                    </select>
                    
                    
                </div>
                <div class="form-group">
                    <label for="firstName" class="" style="width: 10%">Classification <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                      <select name="account_type_header-alt:classification" id="classification" style="width: 35%; display: inline" class="form-control">
                      <!-- <option value="">-pilih-</option> -->
                      <?php foreach ($list_classification as $kh => $header) {
                        echo "<option value='$header[key]'>$header[val]</option>";
                      } ?>
                    </select>
                    <label style="width: 5%; display: inline">&nbsp;</label>
                    <label for="firstName" class="" style="width: 10%">Status <font color="red">*</font></label>
                    <!-- <label style="width: 5%; display: inline">:</label> -->
                    <select name="display-alt:Status" id="status" style="width: 25%; display: inline" class="form-control">
                      <!-- <option value="">-pilih-</option> -->
                      <option value="11">ACTIVE</option>
                      <option value="12">INACTIVE</option>
                      
                    </select>
                    
                    
                </div>
                <div class="form-group">
                <input type="hidden" id="id-txt" name="id">
                <input type="hidden" id="audit_trail-txt" class="print-out" name="audit_trail-nr" value="0">
                <input type="hidden" id="balance_sheet-txt" class="print-out" name="balance_sheet-nr" value="0">
                <input type="hidden" id="cash_flow-txt" class="print-out" name="cash_flow-nr" value="0">
                <input type="hidden" id="profit_loss-txt" class="print-out" name="profit_loss-nr" value="0">

                    <label for="firstName" class="" style="width: 10%">Print Out</label>
                    <label class="checkbox-inline"><input type="checkbox" id="audit_trail" value="1">Audit Trail</label>
                    <label class="checkbox-inline"><input type="checkbox" id="balance_sheet" value="1">Balance Sheet</label>
                    <label class="checkbox-inline"><input type="checkbox" id="cash_flow" value="1">Cash Flow</label>
                    <label class="checkbox-inline"><input type="checkbox" id="profit_loss" value="1">Profit Loss</label>
                    
                </div>
                <div class="form-group">
                    <center>
                      <!-- <a href="javascript:;" class="btn btn-primary">Download</a> -->
                      <button type="button" id="add-btn" class="btn btn-primary" onclick="activateForm()">Add</button>
                      <button type="button" id="save-btn" class="btn btn-primary" >Save</button>
                      <button type="button" id="edit-btn" class="btn btn-primary" onclick="activateForm('edit')">Modify</button>
                      <button type="button" id="cancel-btn" class="btn btn-primary" onclick="activateForm('all', false)">Cancel</button>
                    </center>
                    
                </div>
                </form>
                </div>
			  </div>
  </div>
</div>
<!-- div -->
</div>
<script>
var modifyType = "";
$(".print-out").val("");
$("#message-container").hide();
$("#form-coa input[type=checkbox]").change(function () {
  var idnya = $(this).attr('id');
  var val = $(this).is(':checked');
  // console.log(idnya+" -> "+val);
  $("#"+idnya+"-txt").val((val) ? 1:0);
})
function disabledForm(state, type) {
  if(typeof state === 'undefined') state = true;
  if(typeof type === 'undefined') type = 'add';
  modifyType = type;
  $("#form-coa input,#form-coa select").prop('disabled', state);
  // console.log(state+" -> "+type);
  $("#edit-btn").prop("disabled", state);
  $("#add-btn").prop("disabled", !state);
  $("#save-btn").prop("disabled", state);
  $("#cancel-btn").prop("disabled", state);
  if(type == 'add') $("#id-txt").prop("disabled", true);
  else {
    $("#form-coa input,#form-coa select").prop('disabled', true);
    $("#id-txt").prop("disabled", false);
    $("#add-btn").prop("disabled", true);
    $("#save-btn").prop("disabled", true);
    $("#cancel-btn").prop("disabled", true);
    $("#edit-btn").prop("disabled", false);
    // activateForm('edit')
  }
  
}

function fillForm(data) {
  console.log(data);
  $("#coa_code").val(data.coa_code);
  $("#description").val(data.description);
  $("#header").val(data.is_header == "Header" ? 1:0);
  $("#normal").val(data.normal);
  $("#roll_up").val(data.roll_up_id);
  $("#classification").val(data.classification_id);
  $("#status").val(data.status == 'Active' ? 11:12);
  $("#audit_trail").prop('checked', data.audit_trail == 'Y' ? true:false).trigger('change');
  $("#balance_sheet").prop('checked', data.balance_sheet == 'Y' ? true:false).trigger('change');
  $("#cash_flow").prop('checked', data.cash_flow == 'Y' ? true:false).trigger('change');
  $("#profit_loss").prop('checked', data.profit_loss == 'Y' ? true:false).trigger('change');
  $("#id-txt").val(data.id);
}

function activateForm(type, state) {
    if(typeof state === 'undefined') state = true;
    if(typeof type === 'undefined') type = 'add';
    modifyType = type;
    if(!state) {
      $("#form-coa input").val('');
      $("#form-coa select").prop('selectedIndex', 0);
      table1.$('tr.selected').removeClass('selected');
    }
    $("#form-coa input,#form-coa select").prop('disabled', !state);
    $("#add-btn").prop("disabled", state);
    $("#save-btn").prop("disabled", !state).removeClass((state) ? "btn-primary" : "btn-success").addClass((state) ? "btn-success" : "btn-primary");
    $("#cancel-btn").prop("disabled", !state).removeClass((state) ? "btn-primary" : "btn-danger").addClass((state) ? "btn-danger" : "btn-primary");
    $("#edit-btn").prop("disabled", state);
    
    if(type == 'all') $("#edit-btn").prop('disabled', true)
}
$("#message-container").hide();
function show_alert(type, message) {
  $("#message-container").html(response.message).show("fast");
}

function refresh_roll_up(dt) {
  var list = "";
  $.each(dt, function (key, val) {
    list += "<option value='"+val.nomor+"'>"+val.nomor+" - "+val.name+"</option>";
  })
  $("#roll_up").html(list);
}

$("#save-btn").click(function () {
  swal({
					title: '<?=$confirm_message?>',
					
					type: 'warning',
					showCancelButton: true,
          customClass: 'swal-wide',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'OK'
				}).then(function () {
          $.post("<?= base_url() ?>coa/proses_form/"+modifyType, $("#form-coa").serialize(), function (response) {
            console.log(response);
            if(response.status == -1) {
              $("#message-container").removeClass("alert-success").addClass("alert-danger");
            }
            else{
              $("#message-container").removeClass("alert-danger").addClass("alert-success");
              $("#cancel-btn").click();
              table1.ajax.reload();
              refresh_roll_up(response.roll_up);
              // var new_roll_up = $.parseJSON
            }
              $("#message-container").html(response.message).show("fast");
              setTimeout(() => {
                $("#message-container").html("").hide("fast");
                
              }, 3000);
          }, 'json');
          window.location.reload();
        });
                
                return false;
            });
  
// })
disabledForm();
</script>