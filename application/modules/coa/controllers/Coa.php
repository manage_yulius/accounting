<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	protected $primary_table = "accounting_accounts";
	protected $co_code_session = 0;
	function __construct()
	{
		parent::__construct();
		$this->co_code_session = get_session('co_code');
	}

	public function index()
	{
		$query = "show table status where name = 'accounting_accounts'";
		$exec = $this->db->query($query);
		$res = $exec->row_array();
		echo $res['Engine'];
		// $data['row'] = get_all_data("data_coa", "*", false, "coa_code");
		
		$data['list_parent'] = $this->roll_up_list();
		$data['list_classification'] = get_detail_list("MA0014", "PA_PARENT_CODE = 'COACLASS' and PA_CHILD_VALUE > 0 and CO_CODE = '$this->co_code_session'", "PA_CHILD_VALUE 'key', PA_CHILD_DESC 'val'");
		$data['need_onClick_table'] = true;
		$data['confirm_message'] = get_message('save_confirm');
		
		// echo "<pre>";
		// print_r ($data);
		// echo "</pre>";
		
		$this->page->view('main', $data);
	}

	private function roll_up_list()
	{
		$cek_data = get_count_data("accounting_accounts", "is_header = 1 and display = 11 and CO_CODE = '$this->co_code_session'");
		if($cek_data == 0) {
			insert_data("MA0014", array('CO_CODE' => $this->co_code_session, 'PA_PARENT_CODE' => 'COACLASS', "PA_CHILD_CODE" => 'COACLASS000', "PA_CHILD_DESC" => 'ROOT', 'PA_CHILD_VALUE' => 0));
			insert_data("MA0014", array('CO_CODE' => $this->co_code_session, 'PA_PARENT_CODE' => 'COACLASS', "PA_CHILD_CODE" => 'COACLASS001', "PA_CHILD_DESC" => 'ASSETS', 'PA_CHILD_VALUE' => 1));
			insert_data("accounting_accounts", array('co_code' => $this->co_code_session));
		}
		return get_detail_list("accounting_accounts", "is_header = 1 and display = 11 and CO_CODE = '$this->co_code_session'", "id, nomor, name", 0, -1, false, "nomor");
		
	}

	function data_coa()
	{
		check_ajax_process();
		$data['data'] = get_detail_list("data_coa dc", "CO_CODE = '$this->co_code_session'", "*", 0, -1, false, "coa_code");
		$data['query'] = $this->db->last_query();
		echo json_encode($data);
	}

	public function proses_form($type='add')
	{
		check_ajax_process();
		if($type == 'add') unset($_POST['id']);
		check_validation();
		
		$_POST['co_code'] = $this->co_code_session;
		$data = set_data($this->primary_table);
		// $msg_code = 'save_';
		if($type == 'edit'){
			
			// $msg_code = 'update_';
			$dt['id'] = $data['id'];
			unset($data['id']);
			unset($data['created_by']);
			$data['updated_by'] = get_session('username');
			
			//update data to database and log
			$exec = update_data($this->primary_table, $data, $dt);
		}
		else {
			$coa_code = $this->input->post('nomor', true);
			$n = get_detail_data("accounting_accounts", "co_code = '$this->co_code_session' and nomor = '$coa_code'", "count(*) 'total'");
			$coa_exists = $n['total'];
			//$coa_exists = get_detail_data("accounting_accounts", "co_code = '$this->co_code_session' and nomor = '$coa_code'", "count(*) 'total'")['total'];
			if ($coa_exists > 0) {
				$response['status'] = -1;
				$response['message'] = "COA Code has been registered, please change your COA Code";
				echo json_encode($response);
				exit;
			}
			$data['created_by'] = $this->session->userdata('usr_id');
			$exec = insert_data($this->primary_table, $data);
		}
		$msg_code = ($exec == -1) ? "update_failed" : "save_success";
		$roll_up = $this->roll_up_list();
		$res_message = get_message($msg_code);
		echo json_encode(array('status' => 1, 'message' => $res_message, 'roll_up' => $roll_up));
	}

	function __destruct()
	{
		
	}
}