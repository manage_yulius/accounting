<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Begining extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	protected $co_code_session = 0;
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_begining');
		$this->co_code_session = get_session('co_code');
	}

	public function index()
	{
		// echo $this->co_code_session;die();
		$data['aksi'] = "add";
		$data['row'] = $this->m_begining->list_data();
		$data['accounts'] = get_detail_list("accounting_accounts","display='11' AND CO_CODE = '$this->co_code_session' AND IS_HEADER=0", "*", 0, -1, false, "nomor");
        $data['office'] = $this->gm->get_master_data("MA0014","pa_parent_code='OFFCODE' AND CO_CODE = '$this->co_code_session' AND PA_CHILD_STATUS=11");
        $message_empty = $this->gm->get_master_data("MA0016","MSG_TYPE='empty_field'");
        $data['message_empty'] = trim($message_empty[0]['MSG_ENGLISH']);
        $dc_val = $this->gm->get_master_data("MA0016","MSG_TYPE='dc_validation'");
        $data['dc_val'] = trim($dc_val[0]['MSG_ENGLISH']);
		//$this->load->view('main', $data);
		$this->page->view('main',$data);
	}

	function data_bb()
	{
		$list = $this->m_begining->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $regulasi) {
			$no++;
			$row = array();
			//$row[] = $no;
			$row[] = $regulasi->THN;
			$row[] = $regulasi->COAID."-".$regulasi->name;
			$row[] = $regulasi->PA_CHILD_DESC;
			$row[] = number_format($regulasi->DEBIT,2);
			$row[] = number_format($regulasi->CREDIT,2);
			$row[] = '<center><a  href="begining/edit/'.$regulasi->ID.'" title="Edit"><i class="fa fa-pencil"></i></a></center> ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_begining->count_all(),
			"recordsFiltered" => $this->m_begining->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function tambah_data(){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        $data['aksi'] = "add";
        $accounts = $this->m_begining->get_list_account();
        $data['CategoryID'] = "7";
        $data['category_options'] = $this->m_begining->getAllCategoriesSelector('');
		$data['accounts'] = $accounts;
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function edit($id){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        //$data['office'] = get_all_data("tbl_office");
        $data['accounts'] = get_detail_list("accounting_accounts","display='11' AND CO_CODE = '$this->co_code_session' AND IS_HEADER=0", "*", 0, -1, false, "nomor");
        $data['office'] = $this->gm->get_master_data("MA0014","pa_parent_code='OFFCODE' AND CO_CODE = '$this->co_code_session' AND PA_CHILD_STATUS=11");
		$data['aksi'] = "edit";
		$data['jurnal'] = $this->m_begining->list_data_by_id($id)->row();
		$message_empty = $this->gm->get_master_data("MA0016","MSG_TYPE='empty_field'");
        $data['message_empty'] = trim($message_empty[0]['MSG_ENGLISH']);
         $dc_val = $this->gm->get_master_data("MA0016","MSG_TYPE='dc_validation'");
        $data['dc_val'] = trim($dc_val[0]['MSG_ENGLISH']);
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function detail($id){
		$data['jurnal'] = $this->m_begining->list_jurnal_by_id($id)->row();
		$data['detail'] = $this->m_begining->list_jurnal_detail($id);
		$this->page->view('view',$data);
	}

	function simpan_data(){
		$this->db->trans_begin();
		$co_code = $this->co_code_session;
		if($this->m_begining->save_data($co_code)):
			log_message('info', 'Entry Beginning Balance');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};
		endif;
			$this->session->set_flashdata('Input Beginning Balance Sukses','pesan');
			redirect('begining');
	}

	function delete($id){
		$this->db->trans_begin();
		if($this->m_begining->delete_data($id)):
			$this->activity_log->log('Delete Beginning Balance');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};
			echo(json_encode(array('status'=>'sukses','msg'=>'Data berhasil dihapus.')));
		else:
			echo(json_encode(array('status'=>'sukses','msg'=>'Data gagal dihapus.')));
		endif;
		  redirect('begining');
	}

	function __destruct(){ }
}