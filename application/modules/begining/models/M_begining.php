<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');
class M_begining extends CI_Model{
	
	public $CategoryID;         //:int
    public $ParentCategoryID;     //:int
    public $Category;             //:str
    public $Active;             //:bool
    
    public $select_array;         //:array

    public function __construct() {
        
        parent::__construct();
        
        $this->CategoryID = 0;
        $this->ParentCategoryID = 1;
        $this->Category = '';
        $this->Active = 1;
        
    }

     /**
    * This function gets all the active parent and child categories and returns an array ready for a select menu.
    * @return array
    */
    public function getAllCategoriesSelector($first_option = 'Please select...') {
        
        $sql = "SELECT "
            ."* "
                ."FROM accounting_accounts "
                ."WHERE "
                ."display = ? "
                ." AND parent_id = ? "
            ."ORDER BY nomor";
        
        $params = array(
            1,
            1
        );
        
        $query = $this->db->query($sql, $params);
        
        if ( $first_option != '' ) {
            
            $this->select_array[ '' ] = $first_option;
            
        }
        
        foreach ($query->result_array() as $row) {

            $this->select_array[ $row['id'] ] = $row['name'];
            
            $this->getAllChildCategoriesSelector($row['id']);
            
        }
        
        return $this->select_array;
    }

    /**
    * This function gets all the active child categories and adds to the array.
    * @return void
    */
    public function getAllChildCategoriesSelector($ParentCategoryID) {
        
        $sql = "SELECT "
            ."* "
                ."FROM accounting_accounts "
                ."WHERE "
                ."display = ? "
                ." AND parent_id = ? "
            ."ORDER BY nomor";
        
        $params = array(
            1,
            $ParentCategoryID
        );
        
        $query = $this->db->query($sql, $params);
                
        foreach ($query->result_array() as $row) {
        
            $this->select_array[ $row['id'] ] = '&nbsp;&nbsp;&nbsp;'.$row['name'];
            
        }
        
        
    }

	function list_data() {
        //, tbl_office.name as nama_office
        //, accounting_accounts.name
        $code = get_session('co_code');
        $this->db->select('table_balance.*, accounting_accounts.name, MA0014.PA_CHILD_DESC');
        $this->db->join('accounting_accounts','accounting_accounts.nomor = table_balance.COAID');
        $this->db->join('MA0014','MA0014.PA_CHILD_CODE = table_balance.OFFICEID 
            and MA0014.CO_CODE="'.$code.'"');
        $this->db->from("table_balance");
        $this->db->where('table_balance.CO_CODE="'.$code.'"');
        //$this->db->join('tbl_office','tbl_office.id = table_balance.OFFICEID');
        //$this->db->group_by("accounting_accounts.name");
        return $this->db->get();
    }

    function list_jurnal_by_id($id) {
        return $query = $this->db->query("SELECT * FROM accounting_journal where id='$id'");
    }

    function list_data_by_id($id) {
        $this->db->select('table_balance.*');
        $this->db->where(array('ID'=>$id));
        $this->db->from("table_balance");
        return $this->db->get();
    }
    function get_list_account(){		
		$this->db->order_by('nomor','asc');
		$accounts = $this->db->get('accounting_accounts');
		$data = array();
		$data[''] = '-Pilih Akun-';
		
		foreach($accounts->result_array() as $row):
			$data[$row['id']] = $row['nomor'].'-'.$row['name'];
		endforeach;
		return $data;
	}

    function save_data($co_code){
        $id = $this->input->post('id');
        if($id != 0):
             $data = array('THN'=>$this->input->post('tahun'),'COAID'=>$this->input->post('COAID'), 'OFFICEID'=>$this->input->post('office'),
                'DEBIT'=>$subval = str_replace(',', '',$this->input->post('DEBIT')), 
                'CREDIT'=>str_replace(',', '',$this->input->post('CREDIT')), 
                'CO_CODE'=> $co_code
                );
            $this->db->where('ID',$id);
            $this->db->update('table_balance', $data);
            return $id;
        else:
            $data = array('THN'=>$this->input->post('tahun'),'COAID'=>$this->input->post('COAID'), 'OFFICEID'=>$this->input->post('office'),
                'DEBIT'=>$subval = str_replace(',', '',$this->input->post('DEBIT')), 
                'CREDIT'=>str_replace(',', '',$this->input->post('CREDIT')), 
                'CO_CODE'=> $co_code
                );
            $this->db->insert('table_balance', $data);
            return $this->db->insert_id();
        endif;
    }

  	
    function delete_data($id){
        $this->db->where('ID',$id);
        $this->db->delete('table_balance');
        
        return true;
    }
}
?>