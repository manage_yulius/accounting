   <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        
        <!-- /top tiles -->

        <br />

        <div class="row">
		 			
		<?php echo $this->session->flashdata('pesan');?>
          <div class="col-md-12 col-sm-12 col-xs-12">
		  
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>BEGINNING BALANCE</strong></h2>
				  
                  <div class="clearfix"></div>
                </div>
				<!-- <a type="button" class="btn btn-round btn-success" href="<?php echo base_url('begining/tambah_data');?>"><i class="fa fa-plus-square">&nbsp </i>Tambah Beginning Balance</a> -->
                <div class="x_content">
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <!-- <th>#</th> -->
                        <th>Year</th>                        
                        <th>Chart of Account</th>
                        <!-- <th>Description</th> -->
                        <!-- <th>Normal</th> -->
                        <th>Office</th>
                        <th>Debit</th>
                        <th>Credit</th>
						            <th>Action</th>
                      </tr>
                    </thead>


                     <tbody>
					
					<?php $no = 1; ?>
							<?php foreach($row->result() as $look) { ?>
						<tr> 
							<!-- <td><?php echo $no++; ?></td> -->
							<td><?php echo $look->THN;?></td>							
							<td><?=$look->COAID."-".$look->name; ?></td>
							<td><?=$look->PA_CHILD_DESC; ?></td>
							<td class="text-right"><?=@number_format($look->DEBIT,2); ?></td>
							<td class="text-right"><?=@number_format($look->CREDIT,2); ?></td>	
															
							<td align="center">
								<?php //echo anchor('jurnal/detail/'.$look->id,'<i class="fa fa-bars"></i>',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Details'));?>
								<?php echo anchor('begining/edit/'.$look->ID,'<i class="fa fa-pencil"></i>',array('class'=>'btn btn-info','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Edit'));?>
								<?php //echo anchor('begining/delete/'.$look->ID,'<i class="fa fa-trash"></i>',array('class'=>'btn btn-danger hapus','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Hapus'));?>
							</td>
							
							<?php } ?>
							
						</tr>
						
                      
                    </tbody>
                  </table>
                </div>
        </div>
          <?php $this->load->view('add');?>
            </div>
        </div>
  </div>
		
								
		


       