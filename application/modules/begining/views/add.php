<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script>
   <!-- page content -->
      <div class="form-group" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>Add Beginning Balance</strong></h2>
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="form-journal" data-parsley-validate method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
					<?php validation_errors();?>
                  <div class="row">
                      <div class="col-xs-6">
                          <div class="row">
                          </div>
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Office</label>
                                 <!--  <input class="form-control" type="text" required name="office" value="<?php echo(isset($jurnal->OFFICEID)?$jurnal->OFFICEID:"")?>" /> -->
                                 <select name="office" class="form-control" required >
                                    <?php 
                                    
                                    foreach ($office as $key => $value) { 
                                      if($jurnal->OFFICEID==$value['PA_CHILD_CODE']){
                                        $sel = "selected";
                                      }else{
                                        $sel = "";    
                                      }
                                      ?>
                                      <option value="<?php echo $value['PA_CHILD_CODE']?>" <?php echo $sel?>><?php echo $value['PA_CHILD_DESC']?></option>
                                    <?php }?>
                                 </select>
                                   <input class="form-control" type="hidden" name="id" value="<?php echo(isset($jurnal->ID)?$jurnal->ID:"")?>" />
                              </div>
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Year</label>
                                  <input required class="form-control year" type="text" name="tahun" value="<?php echo(isset($jurnal->THN)?$jurnal->THN:"")?>" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 9)||(event.charCode==46)' maxlength="4" />
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-6 form-group">
                          <label>Chart Of Account</label>
                           <?php 
                           // echo form_dropdown('COAID', $category_options, set_value('id', $CategoryID), array('class'=>"form-control", 'id'=>"account_1"));
                          ?>
                           <select name="COAID" class="form-control" required>
                                    <?php 
                                    
                                    foreach ($accounts as $key => $value) { 
                                      if($jurnal->COAID==$value['name']){
                                        $sel = "selected";
                                      }else{
                                        $sel = "";    
                                      }
                                      ?>
                                      <option value="<?php echo $value['nomor']?>" <?php echo $sel?>><?php echo $value['nomor']?> - <?php echo $value['name']?></option>
                                    <?php }?>
                                 </select>
                      </div>
                      <div class="col-xs-6">
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">DEBIT</label>
                                  <input required class="debit number form-control text-right" type="text" name="DEBIT" value="<?php echo(isset($jurnal->DEBIT)?$jurnal->DEBIT:"0.00")?>" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 9) || (event.charCode==46)|| (event.charCode==0)'/>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">CREDIT</label>
                                  <input required class="credit number form-control text-right" type="text" name="CREDIT"  value="<?php echo(isset($jurnal->CREDIT)?$jurnal->CREDIT:"0.00")?>" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 9) || (event.charCode==46)|| (event.charCode==0)'/>
                              </div>
                          </div>
                      </div>
                      
                  </div>
                  
                   <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                      <button  type="button" name="save" value="Save" class="save btn btn-primary" onclick="add_bb()">Save</button>
                        <?php if($aksi=="edit"):?>
                        <a href="<?php echo base_url('begining');?>" class="btn btn-danger" role="button">Cancel</a>
                      <?php endif;?>
                      </div>
            
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
$(document).ready(function () {
  //$(".numeric").number( true, 0 );
  //masking('.number');
  $(".number").autoNumeric('init', {
      aSep: ',', 
      aDec: '.',
      aForm: true,
      mDec: '2',
      vMax: '999999999999999',
      vMin: '-99999999999999'
  });
});

  function add_bb(){
    
        
          var debit = 0;
          var credit = 0;
          $('input.number').each(function(idx, elm){
              if($(this).val()){
            if($(this).hasClass('debit')){
                debit += parseInt($(this).val());
            }else if($(this).hasClass('credit')) {
                credit += parseInt($(this).val());
            }
              }
          });
          
          /*if(debit==0 && credit==0){
             alert('Jumlah debit dan kredit tidak boleh 0');
               return false;
          }else{
            if(debit == credit) {
               alert('Jumlah debit dan kredit tidak boleh sama : ' + debit + ' != ' + credit);
               return false;
            }else{
              var desc = $(".year").val();
              if(desc==""){
                alert("<?php echo $message_empty?>");
               return false;
              }else{
                
                $(".save").prop("disabled","disabled");
                $(".save").text("processing data...");
                //alert(debit);
                var form = $('#form-journal').get(0);
                var formData = new FormData(form);
                $.ajax({
                  type: 'POST',
                  url: '<?php echo base_url() ?>begining/simpan_data',
                  data: formData,
                  contentType: false, // The content type used when sending data to the server.
                  cache: false,       // To unable request pages to be cached
                  processData:false,
                  success: function(data){
                      window.location = '<?php echo base_url() ?>begining';
                    
                  }
                });
              }
            }     
          }*/
          var desc = $(".year").val();
              if(desc==""){
                alert("<?php echo $message_empty?>");
               return false;
              }else{
                //alert(debit+"~"+credit);
                if((debit>0 && credit==0)||(debit==0 && credit>0)){
                    $(".save").prop("disabled","disabled");
                    $(".save").text("processing data...");
                    var form = $('#form-journal').get(0);
                    var formData = new FormData(form);
                    $.ajax({
                      type: 'POST',
                      url: '<?php echo base_url() ?>begining/simpan_data',
                      data: formData,
                      contentType: false, // The content type used when sending data to the server.
                      cache: false,       // To unable request pages to be cached
                      processData:false,
                      success: function(data){
                        window.location = '<?php echo base_url() ?>begining';
                      }
                    });
                }else{
                  alert("<?php echo $dc_val?>");
                  return false;
                }
                
              
                
          }    
    }
</script>
  


         