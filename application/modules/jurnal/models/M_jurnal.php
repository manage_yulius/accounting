<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_jurnal extends CI_Model
{

    public $CategoryID;         //:int
    public $ParentCategoryID;     //:int
    public $Category;             //:str
    public $Active;             //:bool

    public $select_array;         //:array

    public function __construct()
    {

        parent::__construct();

        $this->CategoryID = 0;
        $this->ParentCategoryID = 1;
        $this->Category = '';
        $this->Active = 1;

    }

    /**
     * This function gets all the active parent and child categories and returns an array ready for a select menu.
     * @return array
     */
    public function getAllCategoriesSelector($first_option = 'Please select...')
    {

        $sql = "SELECT "
            . "* "
            . "FROM accounting_accounts "
            . "WHERE "
            . "display = ? "
            . " AND parent_id = ? "
            . "ORDER BY nomor";

        $params = array(
            1,
            1
        );

        $query = $this->db->query($sql, $params);

        if ($first_option != '') {

            $this->select_array[''] = $first_option;

        }

        foreach ($query->result_array() as $row) {

            $this->select_array[$row['id']] = $row['name'];

            $this->getAllChildCategoriesSelector($row['id']);

        }

        return $this->select_array;
    }

    /**
     * This function gets all the active child categories and adds to the array.
     * @return void
     */
    public function getAllChildCategoriesSelector($ParentCategoryID)
    {

        $sql = "SELECT "
            . "* "
            . "FROM accounting_accounts "
            . "WHERE "
            . "display = ? "
            . " AND parent_id = ? "
            . "ORDER BY nomor";

        $params = array(
            1,
            $ParentCategoryID
        );

        $query = $this->db->query($sql, $params);

        foreach ($query->result_array() as $row) {

            $this->select_array[$row['id']] = '&nbsp;&nbsp;&nbsp;' . $row['name'];

        }


    }

    function list_jurnal()
    {
        $this->co_code_session = get_session('co_code');
        return $query = $this->db->query("SELECT accounting_journal.*, AJ.sum,
                    MA0014.PA_CHILD_DESC, TR0004.PRO_DESC
         FROM accounting_journal
                LEFT JOIN (SELECT accounting_journal_id, SUM(amount) as sum FROM accounting_journal_transaction where type='DEBIT' GROUP BY accounting_journal_id) AJ 
                    ON AJ.accounting_journal_id = accounting_journal.id
                LEFT JOIN MA0014 ON MA0014.PA_CHILD_CODE = accounting_journal.office and MA0014.CO_CODE = '$this->co_code_session'
                LEFT JOIN TR0004 ON TR0004.PRO_CODE = accounting_journal.project AND TR0004.CO_CODE='$this->co_code_session'   
            ");
    }

    function list_jurnal_by_id($id)
    {
        $this->co_code_session = get_session('co_code');
        return $query = $this->db->query("SELECT accounting_journal.*, MA0005.CLI_NAME, MA0014.PA_CHILD_DESC, TR0004.PRO_DESC FROM accounting_journal 
            LEFT JOIN MA0005 ON MA0005.CLI_CODE = accounting_journal.client
            LEFT JOIN MA0014 ON MA0014.PA_CHILD_CODE = accounting_journal.office and MA0014.CO_CODE = '$this->co_code_session'
                LEFT JOIN TR0004 ON TR0004.PRO_CODE = accounting_journal.project AND TR0004.CO_CODE='$this->co_code_session'
            where accounting_journal.id='$id'");
    }

    function list_jurnal_detail($id)
    {
        $this->db->select('accounting_journal_transaction.*');
        $this->db->where(array('accounting_journal_id' => $id));
        $this->db->from("accounting_journal_transaction");
        $this->db->order_by("id");
        //$this->db->join('accounting_accounts','accounting_accounts.id = accounting_journal_transaction.accounting_account_id','LEFT');
        //$this->db->group_by("accounting_accounts.name");
        return $this->db->get();
    }
    function get_list_account()
    {
        $this->db->order_by('nomor', 'asc');
        $accounts = $this->db->get('accounting_accounts');
        $data = array();
        $data[''] = '-Pilih Akun-';

        foreach ($accounts->result_array() as $row) :
            $data[$row['id']] = $row['nomor'] . '-' . $row['name'];
        endforeach;
        return $data;
    }

    function saveJournalTransaction()
    {

        $id = $this->input->post('id');
        if ($id != 0) :
            $this->delete_journal_transaction($id);
        endif;

        $journal_id = $this->saveJournal();
        if ($journal_id) :
            $account_id = $this->input->post('Account');
        $debit = $this->input->post('Debit');
        $no = 0;
        foreach ($debit as $db) :
            if (isset($db['accounting_account_id'])) :
                if($db['amount']>0){
                    $type="DEBIT";
                    $nilai = $db['amount'];
                }else{
                    $type="CREDIT";
                    $nilai = $db['amount_c'];
                }
                //if($db['amount']!=0){
                $this->db->insert('accounting_journal_transaction', array('accounting_journal_id' => $journal_id, 'accounting_account_id' => $db['accounting_account_id'], 'amount' => str_replace(',', '',$nilai), 'type' => $type));
               // }
        endif;
        $no++;
        endforeach;
            
       /* $credit = $this->input->post('Credit');
        $no3 = 0;
        foreach ($credit as $cd) :
            if (isset($cd['accounting_account_id'])) :
                //if($cd['amount']!=0){
                $this->db->insert('accounting_journal_transaction', array('accounting_journal_id' => $journal_id, 'accounting_account_id' => $cd['accounting_account_id'], 'amount' => str_replace(',', '',$cd['amount']), 'type' => 'CREDIT'));
            //}
        endif;
        $no3++;
        endforeach;*/

        return true;
        else :
            return false;
        endif;

    }

    function saveJournal()
    {
        $id = $this->input->post('id');
        $this->co_code_session = get_session('co_code');
        if ($id != 0) :
            $data = array(
            'co_code' => $this->co_code_session,
            'posting_type' => 'MANUAL', 'note' => $this->input->post('description'), 'client' => $this->input->post('client_code'), 'date' => date('Y-m-d', strtotime($this->input->post('date'))), 'user_id' => $this->session->userdata('usr_id'),
            'journal_type' => $this->input->post('type'),
            'project' => $this->input->post('project'),
            'office' => $this->input->post('office'),
            'currency' => $this->input->post('currency'),
            'rate' => str_replace(',', '',$this->input->post('rate'))
        );
        $this->db->where('id', $id);
        $this->db->update('accounting_journal', $data);
        return $id;
        else :
            $data = array(
            'co_code' => $this->co_code_session,
            'posting_type' => 'MANUAL', 'nomor' => $this->input->post('nomor'), 'note' => $this->input->post('description'), 'client' => $this->input->post('client_code'), 'date' => date('Y-m-d', strtotime($this->input->post('date'))), 'user_id' => $this->session->userdata('usr_id'),
            'journal_type' => $this->input->post('type'),
            'project' => $this->input->post('project'),
            'office' => $this->input->post('office'),
            'currency' => $this->input->post('currency'),
            'rate' => str_replace(',', '',$this->input->post('rate')),
            'created' => date('Y-m-d H:i:s')
        );
        $this->db->insert('accounting_journal', $data);
        return $this->db->insert_id();
        endif;
    }

    function delete_journal_transaction($id)
    {
        $this->db->where('accounting_journal_id', $id);
        $this->db->delete('accounting_journal_transaction');
    }

    function delete_journal($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('accounting_journal');

        $this->delete_journal_transaction($id);
        return true;
    }

    function get_last_voucher(){
        $now = date('Ymd');
        $sql=$this->db->query("SELECT MAX(id) ID FROM accounting_journal Where DATE_FORMAT(created,'%Y%m%d')='$now'");
        if($sql->num_rows()>0) {
            $s = $this->db->query("SELECT count(*) as nomor FROM accounting_journal Where DATE_FORMAT(created,'%Y%m%d')='$now'")->row();
         $tmp = ((int)$s->nomor)+1;
         $kd = sprintf("%04s", $tmp);
        }else{
         $kd = "0001";
        }
        return $kd;
    }
}
?>