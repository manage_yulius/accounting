<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script>
   <style type="text/css">
   .ukuran{
    font-size: 0.9em;
   }

   .modal-dialog.modal-800 {
        width: 850px;
        margin: 30px auto;
    }
    input.inputnya { font-size: 13px; }
    select#account_1 { font-size: 13px; }
    .debit_idr { font-size: 13px; }
    .credit_idr { font-size: 13px; }
   </style>
   <!-- page content -->
      <div class="ukuran <?php echo ($aksi=="edit")?'right_col':''?>" role="main" id="stylized">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>Entry Journal</strong></h2>
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="form-journal" data-parsley-validate method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
					<?php validation_errors();?>
                  <div class="row">
                      <div class="col-xs-6">
                          <div class="row">
                              
                          </div>
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Type</label>
                                 <!--  <input class="form-control" type="text" required name="type" value="<?php echo(isset($jurnal->journal_type)?$jurnal->journal_type:"")?>" /> -->
                                 <!-- <select name="type" class="form-control">
                                  <?php 
                                  $pu=$ar=$ot="";
                                  if(isset($journal_type->journal_type)){
                                  switch ($jurnal->journal_type) {
                                    case 'PY':
                                      $pu="selected";
                                      break;
                                    case 'AR':
                                      $ar="selected";
                                      break;
                                    case 'OT':
                                      $ot="selected";
                                      break;                                    
                                    default:
                                     $pu="selected";
                                      break;
                                  }
                                  }
                                  ?>
                                    <option value="PY" <?php echo $pu?>>PY</option>
                                    <option value="AR" <?php echo $ar?>>AR</option>
                                    <option value="OT" <?php echo $ot?>>OT</option>
                                 </select> -->
                                 <select name="type" id="type-list" class="form-control" required>
                                    <?php 
                                    $sel="";
                                    foreach ($type as $key => $value) { 
                                        if(isset($jurnal->journal_type) && $jurnal->journal_type==$value['PA_CHILD_VALUE']){
                                          $sel = "selected";
                                        }else{
                                          $sel = "";    
                                        }
                                    
                                      ?>
                                      <option value="<?php echo $value['PA_CHILD_VALUE']?>" <?php echo $sel?>><?php echo $value['PA_CHILD_DESC']?></option>
                                    <?php }?>
                                 </select>
                                   <input class="form-control" type="hidden" name="id" value="<?php echo(isset($jurnal->id)?$jurnal->id:"")?>" />
                              </div>
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Voucher No.</label>
                                  <input class="form-control" required type="text" name="nomor" value="<?php echo(isset($jurnal->nomor)?$jurnal->nomor:$voucher_nomor)?>" readonly />
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-6">
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Date</label>
                                  <input class="form-control" type="date" name="date" id="date" value="<?php echo(isset($jurnal->date)?$jurnal->date:$back_date)?>"/>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Office.</label>
                                 <select name="office" class="form-control" required>
                                    <?php 
                                    $sel = "";
                                    foreach ($office as $key => $value) { 
                                      if(isset($jurnal)){
                                        if($jurnal->office==$value['PA_CHILD_CODE']){
                                          $sel = "selected";
                                        }
                                      }
                                      
                                      ?>
                                      <option value="<?php echo $value['PA_CHILD_CODE']?>" <?php echo $sel?>><?php echo $value['PA_CHILD_DESC']?></option>
                                    <?php }?>
                                 </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-6 form-group">
                          <label>Project</label>
                          <!-- <input class="form-control" type="text" required name="project"  value="<?php echo(isset($jurnal->project)?$jurnal->project:"")?>"/> -->
                           <select name="project" class="form-control" required>
                                    <?php 
                                    $sel = "";
                                    foreach ($project as $key => $value) { 
                                       if(isset($jurnal)){
                                        if($jurnal->project==$value['PRO_CODE']){
                                          $sel = "selected";
                                        }else{
                                          $sel = "";    
                                        }
                                      }
                                      ?>
                                      <option value="<?php echo $value['PRO_CODE']?>" <?php echo $sel?>><?php echo $value['PRO_DESC']?></option>
                                    <?php }?>
                                 </select>
                      </div>
                     
                      <div class="col-xs-6 form-group">
                          <label>Description</label>
                          <input class="description form-control" type="text" name="description"  value="<?php echo(isset($jurnal->note)?$jurnal->note:"")?>" required/>
                      </div>

                      <div class="col-xs-6">
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Currency</label>
                                  <!-- <input class="form-control" type="text" name="currency"  value="<?php echo(isset($jurnal->currency)?$jurnal->currency:"")?>"/> -->
                                  <select name="currency" id="currency-list" class="form-control" required>
                                    <?php 
                                    $sel="";
                                    foreach ($currency as $key => $value) { 
                                      if(isset($jurnal)){
                                        if(isset($jurnal->currency) && $jurnal->currency==$value['PA_CHILD_VALUE']){
                                          $sel = "selected";
                                        }else{
                                          $sel = "";    
                                        }
                                    }
                                      ?>
                                      <option value="<?php echo $value['PA_CHILD_VALUE']?>" <?php echo $sel?>><?php echo $value['PA_CHILD_VALUE']?></option>
                                    <?php }?>
                                 </select>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                              <label class="col-xs-12">Rate</label>
                                  <input class="numeric form-control text-right" type="text" name="rate" id="rate"  value="<?php echo(isset($jurnal->rate)?$jurnal->rate:"1.00")?>" />
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-4">
                          <label>Client</label>
                         <div class="input-group">
                            <input class="form-control client" placeholder="Search Client" type="text" name="client"  value="<?php echo(isset($jurnal->CLI_NAME)?$jurnal->CLI_NAME:"")?>">
                            <input type="hidden" id="client_code" name="client_code" value="<?php echo(isset($jurnal->client_code)?$jurnal->client_code:"")?>">       
                            <div class="input-group-addon" onclick="open_client()"><i class="fa fa-search"></i></div>
                          </div>
                  </div>
                      <!-- Buat Inisialisasi kalau row pertama dihapus-->
                      <select name="Debit[1][accounting_account_id]" id="account_on" style="display:none">
                          <?php 
                          
                          foreach ($accounts as $key => $value) { 
                           
                            ?>
                            <option value="<?php echo $value['nomor']?>"><?php echo $value['nomor']?> - <?php echo $value['name']?></option>
                          <?php }?>
                       </select>
                  </div>
                  <div style="float:right;">
                  <input class="a_demo_one btn" type="button" value="Add Row" id="btnDebitAdd"> 
                  <!-- <input class="a_demo_one" type="button" value="+ Kredit" id="btnCreditAdd"> -->
                  </div>
                  <div class="row">
                  <div class="x_content">
                  <table id="tbl_journal" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Account</th>                        
                        <th>Debit Amount</th>
                        <th>Debit Amount(<span class="curr-val"><?php echo(isset($jurnal->currency)?$jurnal->currency:"IDR")?></span>)</th>
                        <th>Credit Amount</th>
                        <th>Credit Amount(<span class="curr-val"><?php echo(isset($jurnal->currency)?$jurnal->currency:"IDR")?></span>)</th>
                        
                        <th>Action</th>
                        </tr>
                      </thead>
                    <tbody>
                      <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                       
                      </tr>
                     <?php 
                      $d=0;
                        $dr=$c=$cr=0;
                        $i = 1;
                     if($aksi=="edit"){?> 
                      <?php
                       
                        foreach ($journal_details->result() as $detail):?>
                        <tr id="tr_<?php echo $i?>" class="row_detail">
                          <td id="account_name_1">
                               
                          </td>
                            <td>
                            <?php 
                            if($detail->type == "DEBIT"): ?>
                              <?php 
                                $batch = $detail->accounting_account_id;
                             
                              ?>
                              <select name="Debit[<?php echo $i?>][accounting_account_id]" class="form-control" required id="account_1" style="width:250px">
                                  <?php 
                                  foreach ($accounts as $key => $value) { 
                                    if($detail->accounting_account_id==$value['nomor']){
                                        $sel = "selected";
                                      }else{
                                        $sel = "";    
                                      }
                                  ?>
                                    <option value="<?php echo $value['nomor']?>" <?php echo $sel?>><?php echo $value['nomor']?> - <?php echo $value['name']?></option>
                                  <?php }?>
                               </select>
                            <?php else: ?>
                              <?php
                                $batch = $detail->accounting_account_id;
                              
                              ?>
                              <select name="Debit[<?php echo $i?>][accounting_account_id]" class="form-control" required id="account_1" style="width:250px">
                                  <?php 
                                  foreach ($accounts as $key => $value) { 
                                    if($detail->accounting_account_id==$value['nomor']){
                                        $sel = "selected";
                                      }else{
                                        $sel = "";    
                                      }
                                  ?>
                                    <option value="<?php echo $value['nomor']?>" <?php echo $sel?>><?php echo $value['nomor']?> - <?php echo $value['name']?></option>
                                  <?php }?>
                               </select>
                            <?php endif; ?>
                            </td>
                            
                            <?php if($detail->type == "DEBIT"): 
                              $d=$d+$detail->amount;
                              $dr=$dr+$detail->amount;
                            ?>
                            <td>
                              <input type="text" name="Debit[<?php echo $i?>][amount]" class="inputnya num1 number debit amount required form-control numeric" title="Nominal Debit" id="debit_amount_<?php echo $i?>" style="width:90%;text-align:right" value="<?php echo isset($detail->amount)?$detail->amount:'';?>" nomor="<?php echo $i?>">
                            </td>
                            <td>
                               <input type="text" name="Debit_idr[<?php echo $i?>][amount]" class="inputnya num11 number amount required form-control numeric debit_idr" title="Nominal Debit" id="debit_idr_amount_<?php echo $i?>" style="width:90%;text-align:right" readonly value="<?php echo isset($detail->amount)?($detail->amount*$jurnal->rate):'';?>" nomor="<?php echo $i?>">
                            </td>
                            <td>
                              <input type="text" name="Debit[<?php echo $i?>][amount_c]" class="inputnya num2 number credit amount required form-control numeric" title="Nominal Kredit" id="credit_amount_<?php echo $i?>" style="width:90%;text-align:right" value="" nomor="<?php echo $i?>">
                            </td>
                            <td> <input type="text" name="Credit_idr[<?php echo $i?>][amount_curr]" class="inputnya num22 number amount required form-control numeric credit_idr" title="Nominal Credit" id="credit_idr_amount_<?php echo $i?>" style="width:90%;text-align:right" readonly value="" nomor="<?php echo $i?>">
                            </td>
                            <?php else: 
                              $c=$c+$detail->amount;
                              $cr=$cr+$detail->amount;
                            ?>
                            <td>
                              <input type="text" name="Debit[<?php echo $i?>][amount]" class="inputnya num1 number debit amount required form-control numeric" title="Nominal Debit" id="debit_amount_<?php echo $i?>" style="width:90%;text-align:right" value="" nomor="<?php echo $i?>">
                            </td>
                            <td>
                               <input type="text" name="Debit_idr[<?php echo $i?>][amount]" class="inputnya num11 number amount required form-control numeric debit_idr" title="Nominal Debit" id="debit_idr_amount_<?php echo $i?>" style="width:90%;text-align:right" readonly value="" nomor="<?php echo $i?>">
                            </td>
                            <td>
                              <input type="text" name="Debit[<?php echo $i?>][amount_c]" class="inputnya num2 number credit amount required form-control numeric" title="Nominal Kredit" id="credit_amount_<?php echo $i?>" style="width:90%;text-align:right" value="<?php echo isset($detail->amount)?$detail->amount:'';?>" nomor="<?php echo $i?>">
                            </td>
                            <td> <input type="text" name="Credit_idr[<?php echo $i?>][amount_curr]" class="inputnya num22 number amount required form-control numeric credit_idr" title="Nominal Credit" id="credit_idr_amount_<?php echo $i?>" style="width:90%;text-align:right" readonly value="<?php echo isset($detail->amount)?($detail->amount*$jurnal->rate):'';?>" nomor="<?php echo $i?>"></td>
                            <?php endif; ?>

                            <td>
                              <?php //if($i != 1): ?>
                              <a class="btn btn-danger remove_row" href="#" id="<?php echo $i?>" onclick="remove_row(<?php echo $i?>)"><i class="fa fa-trash"></i></a></a>
                              <?php //endif; ?>
                            </td>
                        </tr>
                          <?php
                        $i++;
                        endforeach;
                          ?>
                     <?php }else{?>
                     <tr id="tr_1" class="row_detail">
                       <td id="account_name_1">
                             
                      </td>
                      <td class="">
                         
                          <select name="Debit[1][accounting_account_id]" class="form-control" required id="account_1" style="width:250px">
                                    <?php 
                                    
                                    foreach ($accounts as $key => $value) { 
                                     
                                      ?>
                                      <option value="<?php echo $value['nomor']?>"><?php echo $value['nomor']?> - <?php echo $value['name']?></option>
                                    <?php }?>
                                 </select>
                      </td>
                     
                      <td>
                          <input type="text" name="Debit[1][amount]" class="inputnya form-control number debit amount required numeric num1" title="Nominal Debit" value='0.00' id="debit_amount_1" style="width:90%;text-align:right" nomor="1">
                      </td>
                      <td>
                           <input type="text" name="Debit_curr[1][amount_curr]" class="form-control number amount numeric required num11 debit_idr" title="Nominal Debit" id="debit_idr_amount_1" readonly style="width:90%;text-align:right" value="0" nomor="1">
                      </td>
                       <td>
                           <input type="text" name="Debit[1][amount_c]" class="inputnya form-control number credit amount required numeric num2" title="Nominal Kredit"  value='0.00' id="kredit_amount_1" style="width:90%;text-align:right" nomor="1" >
                      </td>
                      <td>
                           <input type="text" name="Credit_curr[1][amount_curr]" class="form-control number amount numeric required num22 credit_idr" readonly title="Nominal Kredit" id="kredit_idr_amount_1" style="width:90%;text-align:right" value="0" nomor="1">
                      </td>
                      <td>
                          <a class="btn btn-danger remove_row" href="#" id="1" onclick="remove_row(1)"><i class="fa fa-trash"></i></a></a>
                      </td>
                        </tr>
                      <?php }?>  
                                  
                      </tbody>
                    </table>
                    
                    <table class="table table-striped table-bordered">
                      <tr>
                        <td></td>
                        <td style="width:23%">TOTAL</td>
                        <td>
                          <input type="text" name="jum_debet" id="jum_debet" readonly=readonly class="form-control text-right numeric_total" value="<?php echo $d?>">
                        </td>
                        <td>
                          <input type="text" name="jum_debet_idr" id="jum_debet_idr" readonly=readonly class="form-control text-right numeric_total" value="<?php echo $dr?>">
                        </td>
                        <td>
                          <input type="text" name="jum_kredit" id="jum_kredit" readonly=readonly class="form-control text-right numeric_total" value="<?php echo $c?>">
                        </td>
                        <td>
                          <input type="text" name="jum_kredit" id="jum_kredit_idr" readonly=readonly class="form-control text-right numeric_total"  value="<?php echo $cr?>">
                        </td>
                        <td style="width:6%">&nbsp;</td>
                      </tr>  
                    </table>
                  </div>
                  </div>
                  <div class="form-group">
                    
                    
                    <label class="checkbox-inline"><input class="" type="checkbox" id="print" name="print" value="1" checked="checked">Print Journal Voucher</label>
                    <input type="hidden" name="out_balance" id="out_balance" value="" readonly=readonly class="number" title="">
                  </div>
                   <!-- <div class="ln_solid"></div> -->
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                        <?php //echo form_submit(array('value'=>'Save','name'=>'Simpan','class'=>'btn btn-primary'));?>
                        <button  type="button" name="save" value="Save" class="save btn btn-primary" onclick="add_journal()">Save</button>
                         <?php if($aksi=="edit"):?>
                          <a href="<?php echo base_url('jurnal');?>" class="btn btn-danger" role="button">Cancel</a>
                        <?php endif;?>
                      </div>
            
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog modal-800">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">List Client</h3>
      </div>
      <div class="modal-body">
          
            <table class="table" id="datatable_client" width="90%">
              <thead>
                <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $no=1;
                foreach ($client as $key => $value) {?>
                  <tr class="klik" code="<?php echo $value['CLI_CODE']?>" nama="<?php echo $value['CLI_NAME']?>">
                    <td><?php echo $value['CLI_CODE']?></td>
                    <td><?php echo $value['CLI_NAME']?></td>
                    <td><?php if($value['CLI_STATUS']==11){
                          echo "Active";
                        }else{
                          echo "Suspended";
                        }
                      ?>
                    </td>
                  </tr>
                <?php $no++;}?>
              </tbody>
            </table>
          
       
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Kirim</button>
      </div-->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
      <!--input class="a_demo_one remove_row" type="button" value="Hapus" id="'+idx+'" onclick="remove_row('+idx+')"-->
<script type="text/javascript">
$(document).ready(function () {
  //double click
  $(".klik").dblclick(function(){
    var code = $(this).attr("code");
    var nama = $(this).attr("nama");
     $(".client").val(nama);
     $("#client_code").val(code);

     $('#modal_form').modal('hide');
  });

  //monye format new
  $(".numeric").autoNumeric('init', {
      aSep: ',', 
      aDec: '.',
      aForm: true,
      mDec: '2',
      vMax: '999999999999999',
      vMin: '-999999999999999'
  });
  $(".numeric_total").number( true, 2 );

  document.getElementById("date").min = "<?php echo $back_date?>";
  $(".numeric_rate").number( true, 2 );
 /* $(".numeric").css('cursor','text');
  $(".numeric").number( true, 2 ).css('text-align', 'right').keypress(function () { 
      //console.log($(this));
   });*/

  $('.inputnya').keyup(function(){
      var rate = $("#rate").val().replace(/,/gi,"");
      var num1 = isNaN(parseFloat($(this).parent().parent().find('.num1').val())) ? 0 : parseFloat($(this).parent().parent().find('.num1').val());
      var num2 = isNaN(parseFloat($(this).parent().parent().find('.num2').val())) ? 0 : parseFloat($(this).parent().parent().find('.num2').val());
      var num2El = $(this).parent().parent().find('.num2');
      var num11 = $(this).parent().parent().find('.num11');
      var num22 = $(this).parent().parent().find('.num22');
      var total1 = 0;
      var total2 = 0;
      num1 = $(this).parent().parent().find('.num1').val().replace(/,/gi,"");
      num2 = $(this).parent().parent().find('.num2').val().replace(/,/gi,"");
        var t = parseFloat(num1*rate).toFixed(2);
         num11.val(formatAsMoney(t));
      
        var t = parseFloat(num2*rate).toFixed(2);
         num22.val(formatAsMoney(t));          

         var nomor = $(this).attr("nomor");
          var db = $("#debit_amount_"+nomor).val();
          var cd = $("#kredit_amount_"+nomor).val();
         if(db>0 && cd>0){
          alert('Tidak boleh diisi dua-duanya');
          $(this).val('0');
          countDebt();
          countKred();
          countBalance();
         }
  });

});
  var idx = $('.row_detail').length + 1;
    $('#btnDebitAdd').bind('click', function(e){

      //masking(".number");
      var optionTemplate = $('#account_on').html();
      //if(idx%2==0){
        //var name='Credit['+idx+'][accounting_account_id]';
      //}else{
        var name='Debit['+idx+'][accounting_account_id]';
      //}
      var out ='';
      //out += '<script src="<?php echo base_url()?>assets/js/jquery-number/jquery.number.js">';
      out += '<tr id="tr_'+idx+'">';
      
      out += '<td></td>';
            out += '<td><select name="'+name+'" id="account_'+idx+'" class="required account_id form-control" id="account_'+idx+'" title="Debit" onchange="showAccount('+idx+')"  style="width:250px;font-size:13px;">' + optionTemplate + '</select></td>';
      //out += '<td id="account_name_'+idx+'"></td>';
      out += '<td align="rightx"><input type="text" name="Debit['+idx+'][amount]" class="inputnya num1 debit amount number required form-control numeric2" title="Nominal Debit" id="debit_amount_'+idx+'" style="width:90%; text-align:right;" value="0.00" nomor='+idx+'> </td>';
      out += '<td align="rightx"><input type="text" name="Debit_curr['+idx+'][amount_curr]" class="num11 debit_idr amount number required  form-control numeric2" title="Nominal Debit" id="debit_amount_'+idx+'" style="width:90%; text-align:right;" value="0.00" readonly> </td>';
      out += '<td align="rightx"><input type="text" name="Debit['+idx+'][amount_c]" class="inputnya num2 credit amount number required form-control numeric2" title="Nominal Credit" id="kredit_amount_'+idx+'" style="width:90%; text-align:right;" value="0.00" nomor='+idx+'> </td>';
      out += '<td align="rightx"><input type="text" name="Credit_idr['+idx+'][amount_curr]" class="num22 amount number required credit_idr form-control numeric2" title="Nominal Debit" id="debit_amount_'+idx+'" style="width:90%; text-align:right;" readonly value="0.00"> </td>';
      out += '<td><a href="#" onclick="remove_row('+idx+')" class="btn btn-danger remove_row"><i class="fa fa-trash"></i></a></td>';
            out += '</tr>';
            idx++;
      $('#tbl_journal').append(out);
      
      $('.inputnya').keyup(function(){
       /* $(".numeric2").css('cursor','text');
        $(".numeric2").number( true, 2 ).css('text-align', 'right').keypress(function () { 
          //console.log($(this));
        });
*/
         //monye format new
  $(".numeric2").autoNumeric('init', {
      aSep: ',', 
      aDec: '.',
      aForm: true,
      mDec: '2',
      vMax: '999999999999999',
      vMin: '-999999999999999'
  });

          var rate = $("#rate").val().replace(/,/gi,"");
          var num1 = isNaN(parseFloat($(this).parent().parent().find('.num1').val())) ? 0 : parseFloat($(this).parent().parent().find('.num1').val());
          var num2 = isNaN(parseFloat($(this).parent().parent().find('.num2').val())) ? 0 : parseFloat($(this).parent().parent().find('.num2').val());
          var num2El = $(this).parent().parent().find('.num2');
          var num11 = $(this).parent().parent().find('.num11');
          var num22 = $(this).parent().parent().find('.num22');
          var total1 = 0;
          var total2 = 0;
          num1 = $(this).parent().parent().find('.num1').val().replace(/,/gi,"");
          num2 = $(this).parent().parent().find('.num2').val().replace(/,/gi,"");
         var t = parseFloat(num1*rate).toFixed(2);
         num11.val(formatAsMoney(t));
      
        var t = parseFloat(num2*rate).toFixed(2);
         num22.val(formatAsMoney(t));
           
        var nomor = $(this).attr("nomor");
          var db = $("#debit_amount_"+nomor).val();
          var cd = $("#kredit_amount_"+nomor).val();
         if(db>0 && cd>0){
          alert('Tidak boleh diisi dua-duanya');
          $(this).val('0');
          countDebt();
          countKred();
          countBalance();
         }

      });

     });

     $("#currency-list").change(function () {
       //$(".curr-val").html($(this).children("option").filter(":selected").text());
     })

     $('#stylized').bind('keyup', function(e){
      countDebt();
      countKred();
      countBalance();
      });

     function formatAsMoney(n) {
      n = (Number(n).toFixed(2) + '').split('.');
      return n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.' + (n[1] || '00');
    }
   function countDebt(){
      debt = 0;
      $('.debit').each(function(){
          val = $(this).val().replace(/,/gi,"");
          if(val>0){
            debt += parseFloat(val);
          }
      })
      $('#jum_debet').val(debt);
      debt_idr=0;
      $('.debit_idr').each(function(){
          val = $(this).val().replace(/,/gi,"");
          if(val>0){
            debt_idr += parseFloat(val);
          }
      })
      $('#jum_debet_idr').val(debt_idr);
    //alert(debt);
  }
      
      function countKred(){
        cred = 0;
        $('.credit').each(function(){
            val = $(this).val().replace(/,/gi,"");
            if(val>0){
              cred += parseFloat(val);
            }
        })
        $('#jum_kredit').val(cred);

        cred_idr = 0;
        $('.credit_idr').each(function(){
            val = $(this).val().replace(/,/gi,"");
            if(val>0){
              cred_idr += parseFloat(val);
            }
        })
        $('#jum_kredit_idr').val(cred_idr);
      }
      
      function countBalance(){
        val = parseFloat($('#jum_debet').val()) - parseFloat($('#jum_kredit').val());
        $('#out_balance').val(val);
      }
      
      function remove_row(id){
        $("#tr_"+id).remove();
        countDebt();
        countKred();
        countBalance();
      };

      function open_client() {
        /*var $_base_url = '<?= base_url() ?>';
        sList = window.open($_base_url + "jurnindexal//", "Ubah Transaksi", "width=600,height=500,scrollbars=yes,resizable=yes");*/
        //$('#form')[0].reset(); // reset form on modals
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Cabang'); // Set Title to Bootstrap modal title
      }

      function add_journal(){
    
        
          var debit = 0;
          var credit = 0;
          $('input.amount').each(function(idx, elm){
              if($(this).val()){
            if($(this).hasClass('debit')){
                val = $(this).val().replace(/,/gi,"");
                debit += parseInt(val);
            }else if($(this).hasClass('credit')) {
                val = $(this).val().replace(/,/gi,"");
                credit += parseInt(val);
            }
              }
          });
          var debit = $("#jum_debet").val().replace(/,/gi,"");
          var credit = $("#jum_kredit").val().replace(/,/gi,"");
          console.log(debit+"=="+credit);
          /*if(debit==0 && credit==0){
             alert('Jumlah debit dan kredit tidak boleh 0');
               return false;
          }else if(debit>0 && credit==0){
            alert('Jumlah debit dan kredit tidak boleh 0');
               return false;
          }else{
            if(debit == credit) {
               alert('Jumlah debit dan kredit tidak boleh sama : ' + debit + ' != ' + credit);
               return false;
            }else{*/
              var desc = $(".description").val();
              if(desc==""){
                alert("<?php echo $message_empty?>");
               return false;
              }else if(debit==0 && credit==0){
               alert('Jumlah debit dan kredit tidak boleh 0');
               return false;
              }else{

                if(debit==credit){
                    //alert('sukses');
                    $(".save").prop("disabled","disabled");
                    $(".save").text("processing data...");
                    var form = $('#form-journal').get(0);
                    var formData = new FormData(form);
                    $.ajax({
                      type: 'POST',
                      url: '<?php echo base_url() ?>jurnal/simpan_data',
                      data: formData,
                      contentType: false, // The content type used when sending data to the server.
                      cache: false,       // To unable request pages to be cached
                      processData:false,
                      success: function(data){
                        var val = $("#print").is(':checked');
                        if(val==true){
                          window.location = '<?php echo base_url() ?>jurnal/print_doc/'+data;
                        }else{
                          window.location = '<?php echo base_url() ?>jurnal';
                        }
                      }
                    });
                }else{
                  alert("<?php echo $dc_val?>");
                  return false;
                }
                
              
                
          }  
    }
</script>
  


         