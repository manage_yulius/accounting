   <link href="<?php echo base_url(); ?>assets/gentelella/css/bootstrap.min.css" rel="stylesheet">
   <!-- page content -->
   <style type="text/css">
    @media only screen and (max-width: 700px) {
      .test { font-size: 12px; color: red; }
    }
   </style>
   <?php echo $company[0]['CO_NAME']?>
      <div class="right_col test" role="main" style="font-family:Georgia, serif; color: red; font-size:9px; !important">
        <div class="">
          <div class="clearfix"></div>
          <div class="row-fluid">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <center><h4><strong>JOURNAL VOUCHER</strong></h4></center>
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="demo-form2" data-parsley-validate method="post" action="<?php echo base_url('jurnal/simpan_data');?>" enctype="multipart/form-data" class="form-horizontal form-label-left">
					<?php validation_errors();?>
                  <div class="row">
                  <table width="100%">
                  <tbody>
                    <tr>
                      <td valign="top">
                        <table class="table table-striped table-hover">
                           <tr>
                            <td>Voucher ID</td>
                            <td><a href="#">(<?php echo $jurnal->journal_type?>)&nbsp;<?php echo $jurnal->nomor?></a></td>
                          </tr>
                          <tr>
                            <td>Transaction Date</td>
                            <td><a href="#"><?php echo date('d-m-Y',strtotime($jurnal->date))?></a></td>
                          </tr>
                          <tr>
                            <td>Currency</td>
                            <td><a href="#"><?php echo $jurnal->currency?> - Rate : <?php echo number_format($jurnal->rate,2)?></a></td>
                          </tr>
                          <tr>
                          <td>Client</td>
                          <td><a href="#"><?php echo $jurnal->CLI_NAME?></a></td>
                        </tr>
                         
                        </table>
                      </td>
                      <td valign="top">
                        <table class="table table-striped table-hover">
                          <tr>
                            <td style="width:30%">Print By</td>
                            <td><a href="#"><?php 
                            $sql = $this->db->query("SELECT * FROM users_data where id_user='".$jurnal->user_id."'");
                            echo $sql->row()->username?></a></td>
                          </tr>
                           <tr>
                            <td nowrap>Print Date</td>
                            <td><a href="#"><?php echo date('d-m-Y')?></a></td>
                          </tr>
                           <tr>
                            <td>Office</td>
                            <td><a href="#"><?php echo $jurnal->PA_CHILD_DESC?></a></td>
                          </tr>
                          <tr>
                            <td>Project</td>
                            <td><a href="#"><?php echo $jurnal->PRO_DESC?></a></td>
                          </tr>
                          
                        </table>
                      </td>
                    </tr>
                  </tbody>  
                  </table>
                  </div>
                  <div class="row">
                  <div class="x_content">
                  <div class="row-fluid">
                  <table width="100%">
                  <tbody>
                    <tr>
                      <td valign="top">
                        <table class="table table-striped table-hover" border="0">
                               <tr>
                                <td>Deskripsi</td>
                                <td><a href="#"><?php echo $jurnal->note?></a></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                        </table>
                      </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                  </table>
                  </div>
                  <table id="tbl_journal" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <!-- <th>#</th> -->
                        <td>Account</td>                        
                        <td>Desription</td>                        
                        <td>Debit</td>
                        <td>Debit(<?php echo $jurnal->currency?>)</td>
                        <td>Credit</td>
                        <td>Credit(<?php echo $jurnal->currency?>)</td>
                        
                        <!-- <th>Action</th> -->
                        </tr>
                      </thead>
                    <tbody>
                     
                     
                       <?php $i = 1;
                        $d=0;
                        $dr=$c=$cr=0;
                       foreach ($detail->result() as $detail): ?>
                        <tr id="tr_<?php echo $i?>" class="row_detail">
                          
                          <td width="100px">
                          <?php if($detail->type == "DEBIT"): ?>
                          <?php echo $detail->accounting_account_id;?>
                          <?php else: ?>
                            <?php echo $detail->accounting_account_id;?>
                          <?php endif; ?>
                          </td>
                          <td>
                            <?php if($detail->type == "DEBIT"): ?>
                            <?php 
                            $sql = $this->db->query("SELECT * FROM accounting_accounts where nomor='".$detail->accounting_account_id."'");
                            echo $sql->row()->name;?>
                            <?php else: ?>
                              <?php  $sql = $this->db->query("SELECT * FROM accounting_accounts where nomor='".$detail->accounting_account_id."'");
                            echo $sql->row()->name;?>
                            <?php endif; ?>
                          </td>
                          <!--<td id="account_name_<?php echo $i?>">
                        <?php echo $detail->name; ?>  
                          </td>-->
                          <?php if($detail->type == "DEBIT"): 
                             $d=$d+$detail->amount;
                             $dr=$dr+$detail->amount*$jurnal->rate;
                          ?>
                          <td align="right">
                            <?php echo isset($detail->amount)?number_format($detail->amount,2):'';?>
                          </td>
                          <td align="right">
                          <?php echo isset($detail->amount)?number_format($detail->amount*$jurnal->rate,2):'';?>
                        </td>
                        <td align="right">0.00</td>
                        <td align="right">0.00</td>
                          <?php else: 
                            $c=$c+$detail->amount;
                            $cr=$cr+$detail->amount*$jurnal->rate;
                          ?>
                        <td align="right">
                      0.00
                        </td>
                         <td align="right">
                      0.00
                        </td>
                          <td align="right">
                            <?php echo isset($detail->amount)?number_format($detail->amount,2):'';?>
                          </td>
                          <td align="right">
                            <?php echo isset($detail->amount)?number_format($detail->amount*$jurnal->rate,2):'';?>
                          </td>
                          <?php endif; ?>
                         
                      </tr>
                          <?php
                        $i++;
                        endforeach;
                          ?>
                                  
                        </tbody>
                         <tr>
                            <td style="width:23%" class="text-right">TOTAL</td>
                            <td>&nbsp;</td>
                            <td align="right">
                              <?php echo number_format($d,2)?>
                            </td>
                            <td align="right">
                              <?php echo number_format($dr,2)?>
                            </td>
                            <td align="right">
                              <?php echo number_format($c,2)?>
                            </td>
                            <td align="right">
                              <?php echo number_format($cr,2)?>
                            </td>
                          </tr>  
                      </table>
                      
                    </div>
                  </div>
                  
                  </form>
                </div>
              </div>
            </div>
          </div>
          <table border="1" width="100%">
            <tr>
              <td height="100px" valign="top">Prepared By</td>
              <td valign="top">Checked By</td>
              <td valign="top">Approved By</td>
            </tr>
          </table>
          </div>
          </div>
<script type="text/javascript">
  window.onload = function() { 
    window.print(); 
    window.location = "<?php echo base_url() ?>jurnal";
  }
</script>          
<script type="text/javascript">
$(document).ready(function () {
  $(".numeric").number( true, 2 );
});
</script>
  


         