   <!-- page content -->
      <div class="right_col" role="main">
		<?php $this->load->view('add');?>

       <!-- /top tiles -->
		<?php echo $this->session->flashdata('pesan');?>
        <div class="row">
		 			
          <div class="col-md-12 col-sm-12 col-xs-12">
		  	
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>Journal Entry List</strong></h2>
				  
                  <div class="clearfix"></div>
                </div>
				<!-- <a type="button" class="btn btn-round btn-success" href="<?php echo base_url('jurnal/tambah_jurnal');?>"><i class="fa fa-plus-square">&nbsp </i>Tambah Jurnal</a> -->
                <div class="x_content">
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <!-- <th>#</th> -->
                        <th>Type</th>                        
                        <th>Voucher No.</th>                        
                        <th>Description</th>
                        <th>Project</th>
                        <th>Office</th>
                        <th>Date</th>
						<th>Value</th>
						<th>Status</th>
						
						<th>Action</th>
                      </tr>
                    </thead>


                     <tbody>
					
					<?php $no = 1; ?>
							<?php foreach($row->result() as $look) { ?>
						<tr> 
							<!-- <td><?php echo $no++; ?></td> -->
							<td><?php echo $look->journal_type;?></td>							
							<td><?=$look->nomor; ?></td>
							<td><?=$look->note; ?></td>
							<td><?=$look->PRO_DESC; ?></td>
							<td><?=$look->PA_CHILD_DESC; ?></td>
							<td><?=$look->date; ?></td>	
							<td><?=number_format($look->sum,2); ?></td>	
							<td><? if($look->posted==0){
								echo "Not Posting";
								}else{
									echo "Posting";
								} ?></td>	
															
							<td align="center">
								<?php echo anchor('jurnal/detail/'.$look->id,'<i class="fa fa-bars"></i>',array('class'=>'btn btn-primary','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Details'));?>
								<?php echo anchor('jurnal/edit/'.$look->id,'<i class="fa fa-pencil"></i>',array('class'=>'btn btn-info','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Edit'));?>
								<?php echo anchor('jurnal/delete/'.$look->id,'<i class="fa fa-trash"></i>',array('class'=>'btn btn-danger hapus','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Hapus'));?>
							</td>
							
							<?php } ?>
							
						</tr>
						
                      
                    </tbody>
                  </table>
                </div>
			  </div>
            </div>
        </div>
     </div>
		
								
		


       