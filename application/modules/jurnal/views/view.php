   <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><strong>View Journal</strong></h2>
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="demo-form2" data-parsley-validate method="post" action="<?php echo base_url('jurnal/simpan_data');?>" enctype="multipart/form-data" class="form-horizontal form-label-left">
					<?php validation_errors();?>
                  <div class="row">
                    <table class="table table-striped table-hover">
                      <!-- <thead>
                        <tr>
                          <th>
                            <h4>Field</h4>
                          </th>
                          <th>
                            <h4>Values</h4>
                          </th>
                          
                        </tr>
                      </thead> -->
                      <tbody>
                        <tr>
                          <td style="width:30%">Type</td>
                          <td><a href="#"><?php echo $jurnal->journal_type?></a></td>
                        </tr>
                        <tr>
                          <td>Voucher No</td>
                          <td><a href="#"><?php echo $jurnal->nomor?></a></td>
                        </tr>
                         <tr>
                          <td>Office</td>
                          <td><a href="#"><?php echo $jurnal->PA_CHILD_DESC?></a></td>
                        </tr>
                        <tr>
                          <td>Project</td>
                          <td><a href="#"><?php echo $jurnal->PRO_DESC?></a></td>
                        </tr>
                        <tr>
                          <td>Client</td>
                          <td><a href="#"><?php echo $jurnal->CLI_NAME?></a></td>
                        </tr>
                        <tr>
                          <td>Description</td>
                          <td><a href="#"><?php echo $jurnal->note?></a></td>
                        </tr>
                        <tr>
                          <td>Tanggal</td>
                          <td><a href="#"><?php echo date('d-m-Y',strtotime($jurnal->date))?></a></td>
                        </tr>
                        <tr>
                          <td>Currency</td>
                          <td><a href="#"><?php echo $jurnal->currency?></a></td>
                        </tr>
                        <tr>
                          <td>Rate</td>
                          <td><a href="#"><?php echo number_format($jurnal->rate,2)?></a></td>
                        </tr>
                      
                      </tbody>
                    </table>
                  
                  </div>
                  <div style="float:right;">
                  <!-- <input class="a_demo_one btn" type="button" value="Add Row" id="btnDebitAdd"> 
                  <input class="a_demo_one" type="button" value="+ Kredit" id="btnCreditAdd"> -->
                  </div>
                  <div class="row">
                  <div class="x_content">
                  <table id="tbl_journal" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <!-- <th>#</th> -->
                        <th>Account</th>                        
                        <th>Debit Amount</th>
                        <th>Debit Amount(<?php echo $jurnal->currency?>)</th>
                        <th>Credit Amount</th>
                        <th>Credit Amount(<?php echo $jurnal->currency?>)</th>
                        
                        <!-- <th>Action</th> -->
                        </tr>
                      </thead>
                    <tbody>
                      <tr>
                        <!-- <th>&nbsp;</th> -->
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <!-- <th>&nbsp;</th> -->
                        <!-- <th>&nbsp;</th>
                        <th>&nbsp;</th> -->
                      </tr>
                     
                       <?php $i = 1;
                        $d=0;
                        $dr=$c=$cr=0;
                       foreach ($detail->result() as $detail): ?>
                        <tr id="tr_<?php echo $i?>" class="row_detail">
                          <!-- <td>#</td> -->
                          <td>
                          <?php if($detail->type == "DEBIT"): ?>
                          <?php echo $detail->accounting_account_id;?>
                          <?php else: ?>
                            <?php echo $detail->accounting_account_id;?>
                          <?php endif; ?>
                          </td>
                          
                          <!--<td id="account_name_<?php echo $i?>">
                        <?php echo $detail->name; ?>  
                          </td>-->
                          <?php if($detail->type == "DEBIT"): 
                             $d=$d+$detail->amount;
                             $dr=$dr+$detail->amount;
                          ?>
                          <td>
                        <input readonly type="text" name="Debit[<?php echo $i?>][amount]" class="number debit amount required form-control" digit_decimal="2" digit_length="0" title="Nominal Debit" id="debit_amount_<?php echo $i?>" style="width:90%" value="<?php echo isset($detail->amount)?number_format($detail->amount,2):'';?>">
                          </td>
                          <td>
                           <input type="text" name="Debit_idr[<?php echo $i?>][amount]" class="inputnya num11 number amount required form-control numeric" title="Nominal Debit" id="debit_idr_amount_<?php echo $i?>" style="width:90%" readonly value="<?php echo isset($detail->amount)?(number_format($detail->amount*$jurnal->rate,2)):'';?>">
                        </td>
                        <td></td>
                        <td></td>
                          <?php else: 
                            $c=$c+$detail->amount;
                            $cr=$cr+$detail->amount;
                          ?>
                        <td>
                      &nbsp;
                        </td>
                          <td>
                        
                          </td>
                          <td>
                        <input readonly type="text" name="Credit[<?php echo $i?>][amount]" class="number credit amount required form-control" digit_decimal="2" digit_length="0" title="Nominal Kredit" id="credit_amount_<?php echo $i?>" style="width:90%" value="<?php echo isset($detail->amount)?number_format($detail->amount,2):'';?>">
                          </td>
                          <td> <input type="text" name="Credit_idr[<?php echo $i?>][amount]" class="inputnya num22 number amount required form-control numeric" title="Nominal Credit" id="credit_idr_amount_<?php echo $i?>" style="width:90%" readonly value="<?php echo isset($detail->amount)?(number_format($detail->amount*$jurnal->rate,2)):'';?>"></td>
                          <?php endif; ?>
                         
                      </tr>
                          <?php
                        $i++;
                        endforeach;
                          ?>
                                  
                        </tbody>
                         <tr>
                            <td style="width:23%" class="text-right">TOTAL</td>
                            <td>
                              <input type="text" name="jum_debet" id="jum_debet" readonly=readonly class="form-control text-right numeric" value="<?php echo $d?>">
                            </td>
                            <td>
                              <input type="text" name="jum_debet_idr" id="jum_debet_idr" readonly=readonly class="form-control text-right numeric" value="<?php echo $dr?>">
                            </td>
                            <td>
                              <input type="text" name="jum_kredit" id="jum_kredit" readonly=readonly class="form-control text-right numeric" value="<?php echo $c?>">
                            </td>
                            <td>
                              <input type="text" name="jum_kredit" id="jum_kredit_idr" readonly=readonly class="form-control text-right numeric"  value="<?php echo $cr?>">
                            </td>
                          </tr>  
                      </table>
                      
                    </div>
                  </div>
                   <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                        <?php //echo form_submit(array('value'=>'Simpan','name'=>'Simpan','class'=>'btn btn-primary'));?>
                        <a href="<?php echo base_url('jurnal');?>" class="btn btn-danger" role="button">Back</a>
                      </div>
            
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
$(document).ready(function () {
  $(".numeric").number( true, 2 );
});
  var idx = $('.row_detail').length + 1;
    $('#btnDebitAdd').bind('click', function(e){
      //masking(".number");
      var optionTemplate = $('#account_1').html();

      var out = '';
      out += '<tr id="tr_'+idx+'">';
      out += '<td></td>';
            out += '<td><select name="Account[]" id="account_'+idx+'" class="required account_id form-control" id="account_'+idx+'" title="Debit" onchange="showAccount('+idx+')">' + optionTemplate + '</select></td>';
      //out += '<td id="account_name_'+idx+'"></td>';
      out += '<td><input type="text" name="Debit['+idx+'][amount]" class="debit amount number required form-control" title="Nominal Debit" digit_decimal="2" digit_length="0" id="debit_amount_'+idx+'" style="width:90%;" value="0"> </td>';
      out += '<td><input type="text" name="Debit_idr['+idx+'][amount]" class="debit amount number required form-control" title="Nominal Debit" digit_decimal="2" digit_length="0" id="debit_amount_'+idx+'" style="width:90%;" value="0"> </td>';
      out += '<td><input type="text" name="Credit['+idx+'][amount]" class="debit amount number required form-control" title="Nominal Debit" digit_decimal="2" digit_length="0" id="debit_amount_'+idx+'" style="width:90%;" value="0"> </td>';
      out += '<td><input type="text" name="Credit_idr['+idx+'][amount]" class="debit amount number required form-control" title="Nominal Debit" digit_decimal="2" digit_length="0" id="debit_amount_'+idx+'" style="width:90%;" value="0"> </td>';
      out += '<td><input class="a_demo_one remove_row" type="button" value="Hapus" id="'+idx+'" onclick="remove_row('+idx+')"></td>';
            out += '</tr>';
            idx++;
      $('#tbl_journal').append(out);
     });

   function countDebt(){
      debt = 0;
      $('.debit').each(function(){
          val = $(this).val().replace(/,/gi,"");
          debt += parseFloat(val);
      })
      $('#jum_debet').val(debt);
    //alert(debt);
  }
      
      function countKred(){
        cred = 0;
        $('.credit').each(function(){
            val = $(this).val().replace(/,/gi,"");
            cred += parseFloat(val);
        })
        $('#jum_kredit').val(cred);
      }
      
      function countBalance(){
        val = parseFloat($('#jum_debet').val()) - parseFloat($('#jum_kredit').val());
        $('#out_balance').val(val);
      }
      
      function remove_row(id){
        $("#tr_"+id).remove();
        countDebt();
        countKred();
        countBalance();
      };
</script>
  


         