<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	protected $co_code_session = 0;
	protected $kode_perusahaan = 0;
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jurnal');
		$this->kode_perusahaan = get_session('kode_perusahaan');
		$this->co_code_session = get_session('co_code');
	}

	public function index()
	{
		//echo $this->kode_perusahaan;die();
		$last = $this->m_jurnal->get_last_voucher();
		$data['aksi'] = "add";
		$data['row'] = $this->m_jurnal->list_jurnal();
		$accounts = $this->m_jurnal->get_list_account();
        $data['CategoryID'] = "7";
        $data['category_options'] = $this->m_jurnal->getAllCategoriesSelector('');
        $data['accounts'] = get_detail_list("accounting_accounts","display='11' AND CO_CODE = '$this->co_code_session' AND IS_HEADER=0", "*", 0, -1, false, "nomor");
        $data['office'] = $this->gm->get_master_data("MA0014","pa_parent_code='OFFCODE' AND CO_CODE = '$this->co_code_session' AND PA_CHILD_STATUS=11");
        $data['currency'] = $this->gm->get_master_data("MA0014","pa_parent_code='CURRCODE' AND PA_CHILD_STATUS=11");
        $data['project'] = $this->gm->get_master_data("TR0004","PRO_STATUS=11 AND CO_CODE = '$this->co_code_session'");
        $data['date'] = $this->gm->get_master_data("MA0014","PA_PARENT_CODE='SYSDATE' AND CO_CODE = '$this->co_code_session'");
        $data['type'] = $this->gm->get_master_data("MA0014","PA_PARENT_CODE='TXTYPE' AND PA_CHILD_STATUS=11 AND CO_CODE = '$this->co_code_session'");
        $message_empty = $this->gm->get_master_data("MA0016","MSG_TYPE='empty_field'");
        $data['message_empty'] = trim($message_empty[0]['MSG_ENGLISH']);

         $dc_val = $this->gm->get_master_data("MA0016","MSG_TYPE='dc_validation'");
        $data['dc_val'] = trim($dc_val[0]['MSG_ENGLISH']);
        
        $tgl = $data['date'][0]['PA_CHILD_VALUE'];
        
        //client
        $data['client'] = $this->gm->get_master_data("MA0005","CO_CODE = '$this->co_code_session'");
        
        $time = strtotime($tgl);
		$mysqldate = date('dmY', $time);
		$data['back_date'] = date('Y-m-d', $time);
		$tanggal = date('d/m/Y', $time);
		$data['voucher_nomor'] = $mysqldate."".$last;
        $this->page->view('main',$data);
	}

	function tambah_jurnal(){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        $data['aksi'] = "add";
        $accounts = $this->m_jurnal->get_list_account();
        $data['CategoryID'] = "7";
        $data['category_options'] = $this->m_jurnal->getAllCategoriesSelector('');
		$data['accounts'] = $accounts;
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function edit($id){
		if ($this->session->userdata('level') == 'Admin' ){
		$id_user = $this->session->userdata('id_user');
		$data = (array(
		
			//'queries' => $this->m_admin->get_id_user($id_user)->result(),
			'username'=> $this->session->userdata('username'),		
			'level'=> $this->session->userdata('level'),
			'foto'=> $this->session->userdata('foto'),
			
        ));
        $accounts = $this->m_jurnal->get_list_account();
        $data['CategoryID'] =       "7";
        $data['category_options'] = $this->m_jurnal->getAllCategoriesSelector('');
		$data['accounts'] = $accounts;
		$data['aksi'] = "edit";
		$data['jurnal'] = $this->m_jurnal->list_jurnal_by_id($id)->row();
		$data['journal_details'] = $this->m_jurnal->list_jurnal_detail($id);
		//$data['accounts'] = $this->gm->get_master_data("MA0004","COA_DATA_STATUS='0' AND COA_CLASS=1");
		$data['accounts'] = get_detail_list("accounting_accounts","display='11' AND CO_CODE = '$this->co_code_session' AND IS_HEADER=0", "*", 0, -1, false, "nomor");
        $data['office'] = $this->gm->get_master_data("MA0014","pa_parent_code='OFFCODE' AND CO_CODE = '$this->co_code_session' AND PA_CHILD_STATUS=11");
        $data['currency'] = $this->gm->get_master_data("MA0014","pa_parent_code='CURRCODE' AND PA_CHILD_STATUS=11");
        $data['project'] = $this->gm->get_master_data("TR0004","PRO_STATUS=11 AND CO_CODE = '$this->co_code_session'");
        $data['date'] = $this->gm->get_master_data("MA0014","PA_PARENT_CODE='SYSDATE' AND CO_CODE = '$this->co_code_session'");
        $data['type'] = $this->gm->get_master_data("MA0014","PA_PARENT_CODE='TXTYPE' AND PA_CHILD_STATUS=11 AND CO_CODE = '$this->co_code_session'");
        $message_empty = $this->gm->get_master_data("MA0016","MSG_TYPE='empty_field'");
        $data['message_empty'] = trim($message_empty[0]['MSG_ENGLISH']);

         $dc_val = $this->gm->get_master_data("MA0016","MSG_TYPE='dc_validation'");
        $data['dc_val'] = trim($dc_val[0]['MSG_ENGLISH']);

        $tgl = $data['date'][0]['PA_CHILD_VALUE'];
        
        $time = strtotime($tgl);
		$data['back_date'] = date('Y-m-d', $time);

		//client
        $data['client'] = $this->gm->get_master_data("MA0005","CO_CODE = '$this->co_code_session'");
        
		$this->page->view('add',$data);
		}else{
           redirect('hak_akses/warning');
       }
	}

	function detail($id){
		$data['jurnal'] = $this->m_jurnal->list_jurnal_by_id($id)->row();
		$data['detail'] = $this->m_jurnal->list_jurnal_detail($id);
		$this->page->view('view',$data);
	}

	function print_doc($id){
		$data['jurnal'] = $this->m_jurnal->list_jurnal_by_id($id)->row();
		$data['detail'] = $this->m_jurnal->list_jurnal_detail($id);
		$data['company'] = $this->gm->get_master_data("MA0010","CO_CODE = '$this->kode_perusahaan'");
		$this->load->view('print',$data);
	}

	function simpan_data(){
		$this->db->trans_begin();
		$last = $this->m_jurnal->get_last_voucher();
		
		$date = $this->gm->get_master_data("MA0014","PA_PARENT_CODE='SYSDATE' AND CO_CODE = '$this->co_code_session'");
        $tgl = $date[0]['PA_CHILD_VALUE'];
        $time = strtotime($tgl);
		$mysqldate = date('dmY', $time);

		$_POST['nomor'] = $mysqldate."".$last;
		if($this->m_jurnal->saveJournalTransaction()):
			log_message('info', 'Entry jurnal');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};

		endif;
		$id_last = $this->db->query("SELECT MAX(id) ID FROM accounting_journal")->row();
		$id = $this->input->post('id');
        if ($id != 0) :
        	echo $id;
        else:
			echo $id_last->ID;
		endif;
		
			$this->session->set_flashdata('Input Jurnal Sukses','pesan');
			//redirect('jurnal');
	}

	function delete($id){
		$this->db->trans_begin();
		if($this->m_jurnal->delete_journal($id)):
			log_message('info','Delete jurnal');
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				
			}else{
				$this->db->trans_commit();
				
			};
			//echo(json_encode(array('status'=>'sukses','msg'=>'Data berhasil dihapus.')));
		else:
			//echo(json_encode(array('status'=>'sukses','msg'=>'Data gagal dihapus.')));
		endif;
		  redirect('jurnal');
	}

	function __destruct(){ }
}