<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 
/* Copyright by: VPNKrw */
/* Author: Eryan Fauzan */
/* End of file: auth.php */

class Auth extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		
	}
	
	function test_koneksi()
	{
		$koneksi = mysqli_connect("localhost", "admin", 'accounting@2018', 'insurance_db');
		if (!$koneksi) exit('Gagal Koneksi');
		echo "Koneksi Berhasil";
	}

	function index(){
		if($this->session->userdata('level')=='Admin'){
				redirect('jurnal');
			} else if($this->session->userdata('level')=='Kasir'){
				redirect ('kasir');
			}else{
			$this->load->view('users/login');
			}
	}
	
	function login(){
		$data=(array('username'=>$this->input->post('username'),					 
					 'password'=> sha1($this->input->post('password'))
					 ));

		$data = $this->m_login->get_user($data);
			if($this->session->userdata('level')=='Admin'){
				$credentials = array('usr_id' => $data['id_user'], 'login_number'=> '1');
				$this->session->set_userdata($credentials);
				redirect('users/admin_page');
			} else if($this->session->userdata('level')=='Kasir'){
				redirect ('kasir');
			}else{
			echo "<script>alert('Username Atau Password Salah')</script>";
            redirect('auth','refresh');
			}
		
	}
	

	public function logout() {
		session_unset();
		session_destroy();
		redirect('auth');
	}
	
}
/* Copyright by: VPNKrw */
/* Author: Eryan Fauzan */
/* End of file: auth.php */


?>